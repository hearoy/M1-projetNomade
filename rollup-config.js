var nodeResolve = require('rollup-plugin-node-resolve');
var commonjs = require('rollup-plugin-commonjs');
var globals = require('rollup-plugin-node-globals');
var builtins = require('rollup-plugin-node-builtins');
var uglify = require('rollup-plugin-uglify');
var json = require('rollup-plugin-json');

var rollupConfig = {
   entry: process.env.IONIC_APP_ENTRY_POINT,//'src/app/main.js',
   dest: process.env.IONIC_OUTPUT_JS_FILE_NAME, //'aot/dist/build.js', // output a single application bundle
  sourceMap: process.env.IONIC_GENERATE_SOURCE_MAP ? true : false,
  //sourceMapFile: 'aot/dist/build.js.map',
  format: 'iife',

  plugins: [
     builtins(),
     nodeResolve({ jsnext: true, module: true, browser: true, skip:['exif-js']}),
     {
        resolveId(id, origin) {
           if (id === 'exif-js')
            return 'src/exif.js'
        }
     },
      commonjs({
        include: [
            'node_modules/rxjs/**',
            'node_modules/firebase/**',
           'node_modules/angularfire2/**',
           'node_modules/localforage/**',
            'node_modules/@ionic/storage/**',
        ],
        namedExports: {
        'node_modules/firebase/firebase.js': ['initializeApp', 'auth', 'database'],
        'node_modules/angularfire2/node_modules/firebase/firebase-browser.js': ['initializeApp', 'auth', 'database']
        }
     }),
      globals(),
      json(),
      uglify()
  ],
  useStrict: false,
  paths: {
     'exif-js': 'src/exif.js'
  }
}
module.exports = rollupConfig;

