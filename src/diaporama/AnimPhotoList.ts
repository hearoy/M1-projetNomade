import {DiaporamaListener} from "./DiaporamaListener";
import { MapPage } from '../pages/map/map';
import { OverlayView } from '../components/overlay-view/overlay-view.component';
import { WindowRef } from '../providers/WindowRef';
import { Tools } from '../components/overlay-view/Tools';
import { PhotoDetail } from '../components/overlay-view/PhotoDetail';
import { Diaporama } from './Diaporama';

export class AnimPhotoList {

    private currentStep: number;
    private inProgress: boolean;
    private inPause: boolean;
    private onStop: boolean;

    constructor(
        private windowRef: WindowRef,
        private photoList: PhotoDetail[]
    ){
        this.currentStep = 0;
        this.inPause = false;
        this.next( this );
    }

    public pause(){
        this.inPause = true;
    }

    public resume(){
        this.inPause = false;
    }

    public start(){



    }

    public end(){

    }

    public anim( self, id ){
        if( self.onStop ){
            return;
        }
        if( ! self.inPause ){
            let div = document.getElementById( "anim-" + id );
            div.className += " transition-opacity";

            self.windowRef.nativeWindow.setTimeout(
                self.next.bind(null, self), Diaporama.TIME_PER_PHOTO_SEC * 1000
            );
        }else{
            if( self.currentStep > 0 ){
                self.currentStep--;
            }
        }
    }

    public randomNumberBetween( x, y){
        return Math.floor(Math.random() * y) + x;
    }

    public next( self ){

        if( self.onStop ){
            return;
        }

        if( ! self.inPause ){
            if( self.currentStep == self.photoList.length ){
                //this.tools.updateOverlayListOpen( );
                self.end( );let aboveMap = document.getElementById('aboveMap');
                aboveMap.innerHTML = '';
                return;
            }

            //creer div pour faire l'animation

            var widthWindow = self.windowRef.nativeWindow.innerWidth;
            var heightWindow = self.windowRef.nativeWindow.innerHeight;

            const widthDiv = widthWindow - ( widthWindow * 0.3 );
            const heightDiv = heightWindow - 90;
            const left = ( widthWindow * 0.3 ) / 2;
            const top = 0;


            let aboveMap = document.getElementById('aboveMap');
            aboveMap.innerHTML = '<div style="width:'+widthDiv+'px;height:'+heightDiv+'px;margin-top:'+top+'px;margin-left:'+left+'px;background-image: url(\'' + self.photoList[ self.currentStep ].getUrl() + '\')" id="anim-' + self.currentStep + '" class="photo-animated"><p>'+self.photoList[ self.currentStep ].filename+'</p></div>';

            self.windowRef.nativeWindow.setTimeout(
                self.anim.bind(null, self, self.currentStep), (Diaporama.TIME_BETWEEN_TWO_PHOTO_SEC / 2) * 1000
            );
            self.currentStep++;

        }else{
            self.windowRef.nativeWindow.setTimeout(
                self.next.bind(null, self), 1000
            );
        }
    }

    public stop(){
        let aboveMap = document.getElementById('aboveMap');
        aboveMap.innerHTML = '';
        this.onStop = true;
    }


}
