import {DiaporamaListener} from "./DiaporamaListener";
import { MapPage } from '../pages/map/map';
import { OverlayView } from '../components/overlay-view/overlay-view.component';
import { WindowRef } from '../providers/WindowRef';
import { Tools } from '../components/overlay-view/Tools';
import { AnimPhotoList } from './AnimPhotoList';

export class Diaporama {

    public intervalId: any;

    /**
    *   une étape représente un OverlayView
    *   l'étape actuelle est donc sur quel OverlayView on se trouve.
    **/
    private currentStep: number;
    /**
    *   Représente combien de seconde on reste sur un OverlayView
    *   en seconde
    **/
    static readonly timeStayOnOverlayView = 4;
    private inProgress: boolean;
    private inPause: boolean;
    private timeForCurrentOverlay: number;
    public static TIME_PER_PHOTO_SEC = 8;
    public static TIME_BETWEEN_TWO_PHOTO_SEC = 1;
    private currentAnimation: AnimPhotoList;


    constructor(
        private listener: DiaporamaListener,
        private map: google.maps.Map,
        private tools: Tools,
        private windowRef: WindowRef
    ){
        this.currentStep = 0;
        this.inProgress = false;
        this.inPause = false;
    }

    public getNbreStep( ): number{
        return this.tools.overlayList.length;
    }

    public isPaused( ): boolean{
        return this.inPause;
    }

    private tick( self ){
        if( ! self.inPause ){

            if( self.currentStep == self.tools.overlayList.length ){
                //this.tools.updateOverlayListOpen( );
                MapPage.showToast( "Diaporama fini !", "top", 2000);
                self.end( );
                return;
            }
            //this.tools.updateOverlayListOpen( );
            MapPage.showToast( "Diaporama vers: " + self.currentStep + " !", "top", 2000);

            self.timeForCurrentOverlay = ( Diaporama.TIME_PER_PHOTO_SEC * self.tools.overlayList[ self.currentStep ].photoList.length + Diaporama.TIME_BETWEEN_TWO_PHOTO_SEC * (self.tools.overlayList[ self.currentStep ].photoList.length - 1) ) * 1000;
            self.map.panTo(
                self.tools.overlayList[ self.currentStep ].latlng
            );
            /**this.tools.overlayList[ this.currentStep ].div.click(
                this.tools.overlayList[ this.currentStep++ ].div
            );**/

            self.currentAnimation = new AnimPhotoList(
                self.windowRef,
                self.tools.overlayList[ self.currentStep ].photoList
            );
            self.currentStep++;

            let timer = 500 * Diaporama.timeStayOnOverlayView + self.timeForCurrentOverlay;
            self.windowRef.nativeWindow.setTimeout(
                self.tick.bind(null, self), timer
            );
        }
    }

    /**
    * Démarre le Diaporama et cache l'uid
    * Retourne vrai si le diapo a bien été lancé
    * Sinon retourne faux.
    **/
    public start( ): boolean{
        if( this.tools.overlayList.length < 2 ){
            MapPage.showToast("Pas assez d'image pour commencer un diaporama !", "top", 3000);
            return false;
        }

        if( this.windowRef.nativeWindow != undefined &&
            this.inProgress == false ){
            MapPage.showToast( "Diaporama, on y va ! ", "top", 2000);

            this.inProgress = true;
            this.timeForCurrentOverlay = 0;

            this.windowRef.nativeWindow.setTimeout(
                this.tick.bind(null, this),
                1777
            );

            return true;
        }else{
            console.error("windowRef not loaded to start Diaporama.");
        }
        return false;

    }

    public end( ): void{
        if( this.inProgress ){
            this.windowRef.nativeWindow.clearInterval( this.intervalId );
            this.inProgress = false;
            this.inPause = false;
            this.currentStep = 0;
            this.listener.diaporamaFinished( );
            this.currentAnimation.stop();
        }
    }

    public pause( ): void{
        if( this.inProgress ){
            this.inPause = true;
            this.currentAnimation.pause();
        }
    }

    public resume( ): void{
        if( this.inProgress && this.inPause ){
            this.inPause = false;
            this.currentAnimation.resume();
        }
    }
}
