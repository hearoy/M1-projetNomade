///<reference path="../../../node_modules/@types/googlemaps/index.d.ts" />
///<reference path="../../../myTypes/exif-js.d.ts" />

import { Component, ViewChild, ElementRef } from '@angular/core';
import { FabContainer, ModalController, Toast, ToastController, ToastOptions, Platform } from 'ionic-angular';

import EXIF from 'exif-js';
// import EXIF from '../../exif';
import { ImageWithExif, DetailedPhotosService } from '../../providers/detailed-photos.provider';
import { InputFilesCmp } from '../../components/input-files/input-files.component';
import { GoogleMapsCpnt } from '../../components/google-maps/google-maps.component';
import { AddToAlbumPage } from '../add-to-album/add-to-album.page';
import { AuthService } from '../../providers/auth.service';
import { Camera } from 'ionic-native';
import { FacebookService } from '../../providers/facebook-service.provider';
import { Geolocation } from 'ionic-native';
import { PhotoDetail } from '../../components/overlay-view/PhotoDetail';
import { Tools } from '../../components/overlay-view/Tools';
import { WindowRef } from '../../providers/WindowRef';

import { Diaporama } from '../../diaporama/Diaporama';
import { DiaporamaListener } from '../../diaporama/DiaporamaListener';
import { PhotoOverlayViewPage } from '../photo-overlay-view/photo-overlay-view';
import { FacebookServiceListener } from '../../providers/FacebookServiceListener';
import { AlbumFacebook } from '../add-photo-facebook/AlbumFacebook';
import { AddPhotoFacebookPage } from '../add-photo-facebook/add-photo-facebook.page';




/*
Generated class for the Map page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/

// declare var google: any;


export declare interface PromiseOfArrayBufferAndDataURL { dataURL: Promise<DataURL>, buffer: Promise<ArrayBuffer>, filename: string };


export declare interface ImgWithExifList extends Iterable<ImageWithExif> { };
export declare type DataURL = string;
declare var window: any;


@Component({
    templateUrl: 'map.html'
})
export class MapPage implements DiaporamaListener, FacebookServiceListener{
   @ViewChild('map') mapCpnt: GoogleMapsCpnt;
   @ViewChild('content') contentElt: ElementRef;
   @ViewChild('fab') fabElt: FabContainer;
   @ViewChild('input') inputCmp: InputFilesCmp;

   readonly API_KEY = 'AIzaSyBOSDuYMIq1OPHbX2Vu5AxVsHWr3pGgvBk';
   readonly mapsAPIUrl: string = `https://maps.googleapis.com/maps/api/js?key=${this.API_KEY}`;
   input: HTMLInputElement;
   // private photoList;
   private uid: boolean;
    private diapo: Diaporama;
    public static toastController: ToastController;
   public static tools: Tools;
   private markers;


   constructor(private authService: AuthService,
      private facebookService: FacebookService,
      private modalCtrl: ModalController,
      private photoService: DetailedPhotosService,
      private platform: Platform,
      public toastCtrl: ToastController,
      private winRef: WindowRef) {
      MapPage.tools = new Tools( winRef, this );
      this.markers = [];
      // this.photoList = [];
      this.uid = true;
        MapPage.toastController = toastCtrl;
        this.facebookService.listen( this );
   }

   navigate( ){
        if( this.diapo.start( ) ){
            this.hideUID( );
        }
    }

    pauseNavigate( ){
        if( this.diapo.isPaused( ) ){
            MapPage.showToast(">", "middle", 1000);
            this.diapo.resume( );
        }else{
            MapPage.showToast("||", "middle", 1500);
            this.diapo.pause( );
        }
    }

    stopNavigate( ){
        this.diapo.end( );
        this.showUID( );
    }

    diaporamaFinished( ): void{
        this.showUID( );
    }


    albumListFetched( json: string ): void{
        console.log( json );
    }
    photoFetched( json: string): void{
        console.log( json );

    }
    loginDone( json: string): void{
        MapPage.showToast("Connexion avec Facebook réussi !", "top", 2200);
    }


    photoListFetched( json: string ): void{
        console.log( json );

    }


   //ionViewDidLoad fires just once, when the page is first created, not on subsequent loads from cache
   // All the same, initMap is called on subsequent loads but the view is not ready yet.
   ionViewDidEnter() {
      const mapCompo = this.mapCpnt;
      mapCompo.init();

      this.authService.auth$.subscribe(state => {
         this.getBackFromService$();
      });
      MapPage.tools.setMap( mapCompo.map );
      this.diapo = new Diaporama( this, mapCompo.map, MapPage.tools, this.winRef );

      mapCompo.map.addListener('zoom_changed', function() {
         if( MapPage.tools != undefined && mapCompo.map != undefined ){
            MapPage.tools.updateOverlayListOpen( );
            MapPage.tools.updateZoom( );
         }
      });
      /**
        *   event when user has finished to moved the map
        **/
        mapCompo.map.addListener('dragend', function() {
            MapPage.tools.updateOverlayListOpen( );
        });


        let div = document.createElement('div');
    }



      /**
    * @arg _message: message qui sera affiché
    * @arg _position: position au niveau de l'écran {top, middle, bottom}
    * @arg _duraction: temps en millisecond que le message sera affiché
    **/
    public static showToast(_message, _position, _duration: number) {
        if( MapPage.toastController != undefined ){
            if( MapPage.toastController != undefined ){
                let toast = MapPage.toastController.create({
                    message: _message,
                    duration: _duration,
                    position: _position
                });


                toast.present();
            }
        }else{
            console.log("toastController missing");
        }
    }

    public hideUID( ): void{
        this.uid = false;
    }

    public showUID( ): void{
        this.uid = true;

    }
      goToAddPage(source: string) {
         const modal = this.modalCtrl.create(AddToAlbumPage, { source: source });
         modal.present();
         modal.onDidDismiss(data => {
            if (data)
            this.digestAndDisplay(data.promise, data.album);
        });
    }

    goToOverlayViewPage( photoList: PhotoDetail[][]){
        const modal = this.modalCtrl.create(PhotoOverlayViewPage, { source: photoList });
        modal.present();
        modal.onDidDismiss(data => {
            //closed
        });

    }

    addPhotoFromCamera(): void{

        Camera.getPicture({
            quality : 95,
            destinationType : Camera.DestinationType.DATA_URL,
            sourceType : Camera.PictureSourceType.CAMERA,
            encodingType: Camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        }).then( imageData => {
            //back image64
            let base64Image = "data:image/jpeg;base64," + imageData;
            //get position of the device

            Geolocation.getCurrentPosition({enableHighAccuracy:true, timeout:5000, maximumAge:2000}).then(position => {
                console.log("aé");
                let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                let date: Date = new Date();
                //save to base64 with log position
                let photoDetail = new PhotoDetail(date +"", base64Image, latLng, date );

                this.photoService.photos.push( photoDetail );
                MapPage.tools.setPhotoList( this.photoService.photos );
            }, error => {
                //console.log("ERROR -> " + JSON.stringify(error));
                MapPage.showToast("Erreur, récupération de la position impossible.",
                "middle", 2000);
            });
        }, error => {
            MapPage.showToast("Erreur, lors de la prise de la photo.",
            "middle", 2000);

        });

    }

      addPhotos(source: string) {
         let getImages: Promise<(DataURL | File)[]>;// Promise<DataURL[]|File[]> triggers _TS2349: Cannot invoke an expression whose type lacks a call signature._
         // when doing images.map(...)
         // cf. http://stackoverflow.com/questions/39220876/error-ts2349-cannot-invoke-an-expression-whose-type-lacks-a-call-signature
         switch (source) {
            case 'local':
               getImages = this.inputCmp.getFilesFromLocal();
               break;
         }

         this.digestAndDisplay(getImages);
      }

   fromFacebook(){
        if( this.facebookService.isLogged() ){
            this.facebookService.getAlbumList( this.goToAlbumFacebook, this );
        }else{
            var self = this;
            this.facebookService.doFbLogin( function( response ){
                if( response.status === 'connected' ){
                    self.facebookService.getAlbumList( self.goToAlbumFacebook, self );
                }
            } );
        }
    }

    goToAlbumFacebook( albumListJson, self ){
        let data = albumListJson['albums']['data'];
        let albumList: AlbumFacebook[];
        albumList = [];
        for(var a = 0; a < data.length; a++){
            albumList.push( new AlbumFacebook(
                data[ a ].id,
                data[ a ].created_time,
                data[ a ].name,
                data[ a ]['cover_photo'].id,
                data[ a ].photo_count,
                data[ a ]['cover_photo'].images,
                data[ a ].place
            ) );
        }

        const modal = self.modalCtrl.create(AddPhotoFacebookPage, { albums: albumList });
        modal.present();
        modal.onDidDismiss(data => {
            //closed
        });
    }

   /**
    * Extract geolocation, store in service and create markers and infoWindow's
    * parallel processing, sequential rendering
     */
   private digestAndDisplay(imagesP: Promise<(DataURL | File)[]>, album: string = "") {
      imagesP.then(images => {
         /**
          * map :
          * for each promise of (arrayBuffer, dataURL) corresponding to an image input, promise the EXIFData and the DataURL of the image
          * reduce :
          * don't wait for all images of the array to fullfill, but create a sequence of promises that
          * takes each image and, as soon as both the ExifData and the DataURL are resolved,
          * creates a marker on the map. The markers are created sequentially.
          */
         const parallel = images.map(image => this.photoService.digestPhoto(image));
         Promise.all(parallel).then(imgArray => this.photoService.addBundleToAlbum$(album));
         const seq = parallel.reduce(
            (acc, detailedImgPromise) => {
               return acc.then(() => {
                  detailedImgPromise.then(detailedImg => {
                     const albumTitle = this.photoService.computeAbumTitle(album);
                     // this.img2marker(detailedImg, albumTitle);
                     this.photoService.convert2Photodetails();
                     MapPage.tools.setPhotoList( this.photoService.photos );
                  })
               })
            }, Promise.resolve()
         );
      });
   }


   private getBackFromService$() {
      const toast = this.showToast1('Pouring from the clouds (the local ones) ...');
      setTimeout(() => toast.dismiss(), 10000);
      // this.photoService.albums$.forEach(album => album.photos.forEach(
      //    detailedImg => this.img2marker(detailedImg, album))
      // );
      this.photoService.albums$.multicasted
         .subscribe({
            next: album => {
               toast.setMessage(`It's raining photos!`);
               setTimeout(() => toast.dismiss(), 1000);
               //album.photos.forEach(detailedImg => this.img2marker(detailedImg, album.title));
            },
            error: err => console.log(`getBackFromService$ -> album$ error : ${err}`),
            complete: () => {
               console.log(`in MapPage, album$.subject completed !?`);
               toast.dismiss();
               const toast2 = this.showToast1('All done!');
               setTimeout(() => toast2.dismiss(), 2000);

               this.photoService.convert2Photodetails();

               MapPage.tools.setPhotoList(this.photoService.photos);
            }
         });
         // .pluck<ImageForMap[]>('photos')
         // .subscribe(
         // {
         //    next: photos => {
         //       this.photoService.photos.push(...photos);
         //    }
         // });
   }


   private img2marker(detailedImg, album:string) {
      const map = this.mapCpnt;
      const marker = map.createMarker(detailedImg);
      this.markers.push(marker);
      let otherDetails : Map<string,string> = new Map();
      otherDetails.set('album', this.photoService.computeAbumTitle(album));
      const infoWindow = this.mapCpnt.createInfoWindow(detailedImg,otherDetails);
      marker.addListener('click', () => map.openInfoWindow(marker, infoWindow));
   }


    getFromFacebook(): Promise<PromiseOfArrayBufferAndDataURL[]> {//PromiseOfArrayBufferAndDataURL[] {
        return null;
    }


    geoLocImage(buffer: ArrayBuffer): EXIF.ExifData {
        return EXIF.readFromBinaryFile(buffer);
    }


   logIn() {
      this.authService.signInWithGoogle().then(this.onSignedIn.bind(this));
   }
   logOut() {
      this.authService.signOut();
   }

   onSignedIn() {
      //this.getBackFromService$();
   }

   isLoggedOut() {
      return !this.authService.authenticated;
   }

   save() {
      // this.remoteDBService.saveAlbum(this.photoService.albums$[0]);
      const toast = this.showToast1('Backing up...');
      this.photoService.save().subscribe({
         complete: () => {
            toast.dismiss();
         }
      });
   }

   showToast1(msg: string): Toast {
      const opts: ToastOptions = {
         message: msg,
         position: 'bottom',
         dismissOnPageChange: false
      }
      const toast = this.toastCtrl.create(opts);
      toast.present();
      return toast;
      }

   drawPoly() {
      const path: google.maps.LatLngLiteral[] = [
         { lat: 47.845375, lng: 1.939458 },
         { lat: 47.390239, lng: 0.726906 },
         { lat: 47.976539, lng: 2.727891 }
      ]
      this.mapCpnt.drawPolyline(path);
   }

   drawBetweenAlbums() {
      let album0 = this.photoService.albums[0];
      let mean0 = this.photoService.getMean(album0);
      this.mapCpnt.createMarker(null, mean0.value);
      let album1 = this.photoService.albums[1];
      let mean1 = this.photoService.getMean(album1);
      this.mapCpnt.createMarker(null, mean1.value);
      //debugger;
      const path: google.maps.LatLng[] = [mean0.value, mean1.value];
      this.mapCpnt.drawPolyline(path);

   }


};
