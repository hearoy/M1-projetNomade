import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { DetailedPhotosService, Album } from '../../providers/detailed-photos.provider';
import { PhotoDetail } from '../../components/overlay-view/PhotoDetail';

/*
Generated class for the AddToAlbumPage page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/
@Component({
    templateUrl: 'photo-overlay-view.html'
})
export class PhotoOverlayViewPage {

    private photoList: PhotoDetail[][];
    private photoSubList;

    constructor(
        public viewCtrl: ViewController,
        navParams: NavParams
    ){
        this.photoList = navParams.get('source');
    }

    ionViewDidLoad() {
        let subArray = [];
        for(var dateIndex = 0; dateIndex < this.photoList.length; dateIndex++ ){
            for(var p = 0; p < this.photoList[ dateIndex ].length; p += 3 ){
                let row = [];
                for(var i = 0; i < 3; i++){
                    var value = this.photoList[ dateIndex ][ p + i ];
                    if( ! value ){
                        break;
                    }
                    row.push( value );
                }
                if( subArray[ dateIndex ] == undefined ){
                    subArray[ dateIndex ] = [];
                }
                subArray[ dateIndex ].push( row );
            }
        }
        this.photoSubList = subArray;
    }

    close() {
        this.viewCtrl.dismiss();
    }

    escapeHtml(unsafe) {
        return unsafe.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;").replace(/'/g, "&#039;");
    }


}
