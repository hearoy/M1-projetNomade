import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the PhotosDetails page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-photos-details',
  templateUrl: 'photos-details.html'
})
export class PhotosDetailsPage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello PhotosDetails Page');
  }

}
