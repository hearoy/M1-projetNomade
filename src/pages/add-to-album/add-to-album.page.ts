import { Component, ViewChild, ElementRef } from '@angular/core';
import { TextInput, NavParams, ViewController } from 'ionic-angular';
import { InputFilesCmp } from '../../components/input-files/input-files.component';
import { DetailedPhotosService, Album } from '../../providers/detailed-photos.provider';

/*
  Generated class for the AddToAlbumPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
   templateUrl: 'add-to-album.page.html'
})
export class AddToAlbumPage {
   @ViewChild('titleInput') titleInput: TextInput;
   @ViewChild(InputFilesCmp) inputFiles: InputFilesCmp;
   albums: Album[];
   promise: Promise<File[]>;
   source: string;
   title: Promise<string>;

   constructor(public viewCtrl: ViewController, navParams: NavParams, public photosService: DetailedPhotosService) {
      this.albums = photosService.albums$;
      this.source = navParams.get('source');
   }

   ionViewDidLoad() {
      // TODO source -> Enum
      if (this.source.search('local') > -1) {
         this.promise = this.inputFiles.getFilesFromLocal();
      } else { //facebook
      }
   }

   addTo(title: string) {
      const titleRegEx = title.match(/new-(.*)/);
      if(titleRegEx !== null)
         if (this.albums.some(album => album.title == titleRegEx[1]))
            title = titleRegEx[1];
      this.viewCtrl.dismiss({ promise: this.promise, album: title });
   }

   cancel() {
      this.viewCtrl.dismiss();
   }

   _titleIsEmpty(): boolean {
      const nativeInput = <HTMLInputElement>this.titleInput.getNativeElement(); //attempt
      const res = !this.titleInput.value.length && this.titleInput.value.trim().length == 0;
      return res;
   }

}
