import { Component, ViewChild, ElementRef } from '@angular/core';
import { TextInput, NavParams, ViewController, LoadingController  } from 'ionic-angular';
import { InputFilesCmp } from '../../components/input-files/input-files.component';
import { DetailedPhotosService, Album } from '../../providers/detailed-photos.provider';
import { FacebookService } from '../../providers/facebook-service.provider';
import { AlbumFacebook } from './AlbumFacebook';
import { PhotoDetail } from '../../components/overlay-view/PhotoDetail';
import { MapPage } from '../map/map';

/*
Generated class for the AddToAlbumPage page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/
@Component({
    templateUrl: 'add-photo-facebook.page.html'
})
export class AddPhotoFacebookPage {
    @ViewChild('titleInput') titleInput: TextInput;
    @ViewChild(InputFilesCmp) inputFiles: InputFilesCmp;
    albums: Album[];
    promise: Promise<File[]>;
    albumsFacebook: AlbumFacebook[];
    title: Promise<string>;
    subArray;
    selectAlbum: boolean;
    private albumSelected: Number[];
    private albumSelectedPositionError: Number[];
    loading;

    constructor(
        public viewCtrl: ViewController,
        navParams: NavParams,
        public photosService: DetailedPhotosService,
        private facebookService: FacebookService,
        public loadingCtrl: LoadingController,
        private photoService: DetailedPhotosService
    ) {
        this.albumsFacebook = navParams.get('albums');
        for(var a = 0; a < this.albumsFacebook.length; a++){
            if( this.facebookService.albumListAdded.indexOf( this.albumsFacebook[ a ].getId() ) != -1 ){
                this.albumsFacebook.splice(a, 1);
            }
        }
        this.selectAlbum = true;
        this.albumSelected = [];
        this.albumSelectedPositionError = [];
        this.loading = loadingCtrl.create({
            content: 'Chargement..',
            spinner: 'crescent'
        })

    }

    showLoading(){
        this.loading.present();
    }

    hideLoading(){
        this.loading.dismiss();
        this.loading = this.loadingCtrl.create({
            content: 'Chargement..',
            spinner: 'crescent'
        })
    }

    getArrayOfArray( ){
        return this.subArray;
    }

    getAlbumFromId( id: Number ): AlbumFacebook{
        for(var a = 0; a < this.albumsFacebook.length; a++){
            if( this.albumsFacebook[ a ].getId() == id){
                return this.albumsFacebook[ a ];
            }
        }
        return undefined;
    }

    albumClicked( id: Number ){
        //album séléctionné, on va fetch les photos qui sont dedans
        if( this.albumSelectedPositionError.indexOf( id ) != -1){
            this.albumSelectedPositionError.splice(this.albumSelectedPositionError.indexOf( id ), 1);
        }else if( this.albumSelected.indexOf( id ) == -1 ){
            let currentAlbum = this.getAlbumFromId( id );
            if( currentAlbum != undefined ){
                if( currentAlbum.getPlace() == undefined ){
                    //aucune position set sur l'album, on va check si une photo
                    //contient une position pour l'attribuer à l'album
                    this.showLoading();
                    var self = this;
                    this.facebookService.getPhotoFromAlbumId( id, function( response ){
                        //on parcours chaque photo recu
                        let data = response['data'];
                        for(var d = 0; d < data.length; d++){
                            if( data[ d ].place != undefined ){
                                //une photo a une position, on l'a prend par defaut
                                //pour l'album
                                currentAlbum.setPlace( data[ d ].place );
                                break;
                            }
                        }
                        if( currentAlbum.getPlace() == undefined ){
                            //aucune position trouvée sur une photo
                            //erreur on avertis l'user
                            self.albumSelectedPositionError.push( id );
                        }else{
                            //position set
                            //on en profite pour save les images dans l'album
                            currentAlbum.setImages( data );
                            self.albumSelected.push( id );
                        }
                        self.hideLoading();
                    });
                }else{
                    this.albumSelected.push( id );
                }
            }
        }else{
            this.albumSelected.splice(this.albumSelected.indexOf( id ), 1);
        }

    }

    albumIsSelected( id: Number ){
        if( this.albumSelected.indexOf( id ) != -1 ){
            return true;
        }
        return false;
    }

    getClassForAlbum( id: Number ){
        if( this.albumSelected.indexOf( id ) != -1 ){
            return 'album-selected';
        }else if( this.albumSelectedPositionError.indexOf( id ) != -1 ){
            return 'album-pos-unknow';
        }
        return '';
    }

    addAlbum(){
        //parcourir les albums séléctionnés
        //ajouter la position sur une image si elle est manquante
        //convertir chaque image en base64
        //creer des photoDetail
        //mettre à jour la liste
        for(var a = 0; a < this.albumSelected.length; a++){
            let currentAlbum = this.getAlbumFromId( this.albumSelected[ a ] );
            this.facebookService.albumListAdded.push( currentAlbum.getId() );
            //on va parcourir les photos pour être sûr qu'elles aient toutes
            //une position, et les convertir en photoDetail
            let images = currentAlbum.getImages();
            var done = 0;
            this.showLoading();
            for(var i = 0; i < images.length; i++){
                //si une image n'a pas de position on lui met celle de l'album
                let currentImage = images[ i ];
                var self = this;
                this.setBase64FromUrl( currentImage['images'][ 0 ].source, function( base64 ){

                    if( currentImage.name === undefined || currentImage.name === ''){
                        currentImage.name = new Date();
                    }
                    if( currentImage.place === undefined || currentImage.place === ''){
                        currentImage.place = currentAlbum.getPlace();
                    }
                    if( currentImage.created_time === undefined || currentImage.created_time === ''){
                        currentImage.created_time = currentAlbum.getCreatedTime();
                    }

                    let latLng = new google.maps.LatLng(
                        currentImage.place['location'].latitude,
                        currentImage.place['location'].longitude
                    );

                    let photoDetail = new PhotoDetail(
                        currentImage.name,
                        base64,
                        latLng,
                        new Date( currentImage.created_time )
                    );

                    self.photoService.photos.push( photoDetail );
                    //update overlay-view
                    MapPage.tools.setPhotoList( self.photoService.photos );
                    done++;
                    if( done == images.length){
                        //toutes les images ont été traitées
                        //fermer cette  page
                        self.hideLoading();
                        self.viewCtrl.dismiss();
                    }
                });
            }
        }
    }

    setBase64FromUrl( url: string, callback){
        let img = new Image();
        img.onload = function(){
            var canvas = document.createElement("canvas");
            canvas.width = img.width;
            canvas.height = img.height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);
            var dataURL = canvas.toDataURL("image/jpg");
            //console.log( dataURL.replace(/^data:image\/(png|jpg);base64,/, "") );
            callback( dataURL );
            //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
        }
        img.crossOrigin = 'Anonymous';
        img.src = url;
    }


    escapeHtml(unsafe) {
        return unsafe.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;").replace(/'/g, "&#039;");
    }

    ionViewDidLoad() {
        var subArray = [];
        for(var a = 0; a < this.albumsFacebook.length; a += 2 ){
            var row = [];
            for(var i = 0; i < 2; i++){
                var value = this.albumsFacebook[ a + i ];
                if( ! value ){
                    break;
                }
                row.push( value );
            }
            subArray.push( row );
        }
        this.subArray = subArray;
    }

    cancel() {
        this.viewCtrl.dismiss();
    }


}
