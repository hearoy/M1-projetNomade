export class AlbumFacebook{

    private imageCoverList;
    private place;
    /**
    * "place": {
    "name": "Lizeray",
    "location": {
    "city": "Lizeray",
    "country": "France",
    "latitude": 46.9667,
    "longitude": 1.9
},
"id": "108281975860635"
},
**/
    private images;
constructor(
    private id: Number,
    private created_time: string,
    private name:string,
    private coverId: Number,
    private photoCount: Number,
    imageList,
    place){
        this.imageCoverList = imageList;
        this.place = place;

    }

    getCreatedTime(){
        return this.created_time;
    }

    setImages( images ){
        this.images = images;
    }

    /**
    *   Liste des images contenues
    *   dans l'album
    **/
    getImages(){
        return this.images;
    }

    /**
    *   Liste des différentes représentations
    *   de l'image de cover de l'album.
    **/
    public getCoverImages(){
        return this.imageCoverList;
    }


    getId(){
        return this.id;
    }

    getPlace(){
        return this.place;
    }

    setPlace( place ){
        this.place = place;
    }
    /**
    *   Retourne la représentation de l'image
    *   la plus petit du cover de l'album
    *
    **/
    public getSmallestImage(){
        let minWidth = 9999;
        let minImage;
        for(var i = 0; i < this.imageCoverList.length; i++){
            if( this.imageCoverList[ i ].width < minWidth ){
                minImage = this.imageCoverList[ i ];
            }
        }
        return minImage;
    }
}
