import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { ItemDetailsPage } from '../item-details/item-details';
import { Album, DetailedPhotosService } from '../../providers/detailed-photos.provider';


@Component({
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  albums: Array<Album>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public servicePhoto: DetailedPhotosService) {
     this.albums = servicePhoto.albums$;
  }

  goToAlbum(event, albumId: number) {
     this.navCtrl.push(this.albums[albumId]);
   }

}
