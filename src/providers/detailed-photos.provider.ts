import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { ConnectableObservable, Observable, ReplaySubject, Subject } from 'rxjs';
import EXIF from 'exif-js';
import { DataURL, MapPage } from '../pages/map/map';
import { RemoteDBService, ImageForMap4DB } from '../providers/remote-db.service';
import { LocalDBService } from '../providers/local-db.service';

import { PhotoDetail } from '../components/overlay-view/PhotoDetail';


export declare interface ImageWithExif {
    exifData: EXIF.ExifData,
    url: DataURL,
    filename: string
};

export declare interface Album {
    id: number,
    title: string,
    photos: ImageForMap[] //ImageWithExif[];
}
export declare interface ImageForMap extends ImageWithExif{
    album: string,
    albumId: number,
    latLng: google.maps.LatLng | google.maps.LatLngLiteral
}

/**
* A mirror of a photo, used for sorting by and averaging coordinates
*/
declare interface PhotoProxyItem {
    /**
    * index of the containing album
    */
    albumId: number,

    /**
    * original index of the mirrored photo
    */
    idx: number,

    /**
    * copy of the mirrored photo's coordinates
    */
    latLng: google.maps.LatLng | google.maps.LatLngLiteral
};

/**
* Array of PhotoProxyItem's
* with additional average coordinates and associated weight
* (used to lighten calculation when adding new photos)
*/
declare interface PhotoProxy extends Array<PhotoProxyItem> {
    mean: { value: google.maps.LatLng, weight: number }
};

@Injectable()
export class DetailedPhotosService {
    private _albums: Album[];
    private currentAlbum: Album;
    /**
    * index of the first photo that is not in album yet
    */
   private idxStart: number;
   private nameIdx = 0;
   public photos: ImageForMap[]; //ImageWithExif[];
   private proxies: PhotoProxy[];

   private photos$: Observable<ImageForMap>;
   private _albums$: MyAlbums$;
   private _albums$ubj: Subject<Album>;
   private fromDB = false;

   constructor(private rDBservice: RemoteDBService, private locDBService: LocalDBService) {
      this.photos = [];
      this._albums = [];
      this.idxStart = 0;
      this.proxies = [];
      this._albums$ubj = new Subject<Album>();
      this._albums$ = new MyAlbums$(this._albums$ubj);
   }

   get albums() {
      return this._albums;
   }
   get albums$() {
      if (this.fromDB)
         this._albums$;
      else
         this._albums$ = this.getAlbumsBackFromLocal();
      return this._albums$;
   }

   getAlbumsBackFromRemote(): MyAlbums$ {
      const albums$ = this.rDBservice.download();
      this._albums$ = albums$;
      const photos$ = albums$.multicasted.pluck<MyPhotos$>('photos');
      const photosLength$ = photos$
         .scan<number>((length, photos) => { return length += photos.length }, this.idxStart);
      photosLength$.subscribe(length => this.idxStart = length);
      photos$.subscribe({
         next: photos => { this.photos.push(...photos); }
      });
      return albums$;
      // const obs1 = albums$.map(album => {
      //    this._albums[album.id] = album;
      //    return album;
      // });
      // const photos$ = obs1.mergeMap<ImageForMap>(album => Observable.from(album.photos));
      // photos$.map(img => this.photos.push(img));
   }

   getAlbumsBackFromLocal() {
      this.fromDB = true;
      const albums$ = this.locDBService.download();
      this._albums$ = albums$;
      const photos$ = albums$.multicasted.pluck<MyPhotos$>('photos');
      const photosLength$ = photos$
         .scan<number>((length, photos) => { return length += photos.length }, 0);
      photosLength$.subscribe(length => this.idxStart = length);
      photos$.subscribe({
         next: photos => { this.photos.push(...photos); }
      });
      return albums$;
   }

   addBundleToAlbum(title: string) {
      const albums = this._albums;
      // const albums = this.albums$;
      let albumTitle = title;
      let match = title.match(/new\-(.*)/);
      const photos2add = this.photos.slice(this.idxStart);
      if (match !== null) {
         albumTitle = match[1];
      } else if (title = '') { // unused
         albumTitle = `default-${this.nameIdx++}`;
      } else {
         for (let album of albums) {
            if ( album.title.match(albumTitle) ) {
               this.currentAlbum = album; //currently unused
               // update the album with the new photos
               album.photos.push(...photos2add);
               photos2add.forEach(photo => {
                  photo.album = albumTitle;
                  photo.albumId = album.id;
               });
               // retrieve the album's proxy and update it
               const proxy = this.proxies[album.id];
               const proxyItems2add = this.createProxy(photos2add);
               this.sortedInsert<PhotoProxyItem>(proxy, this.compareLatLng, ...proxyItems2add);
               proxy.mean= this.meanLatLngAccu(proxy, proxyItems2add);
               // just in case... but shouldn't be required
               // this.proxies[album.id] = proxy;
               this.idxStart = this.photos.length;
               return album.title;
            }
         }
      }
      const album = {
         photos: photos2add,
         title: albumTitle,
         id: this.albums.length
      };
      const proxy = <PhotoProxy>[];
      album.photos.forEach((photo, idx) => {
         photo.album = albumTitle;
         photo.albumId = album.id;
         this.sortedInsert(proxy, this.compareLatLng, ...this.createProxy([photo], idx));
      });
      this.proxies[album.id]=proxy;
      proxy.mean = { value: this.meanLatLng(photos2add), weight: photos2add.length };
      this.currentAlbum = album;
      this.idxStart = this.photos.length;
      this.albums.push(album);
      return albumTitle;
   }

   addBundleToAlbum$(title: string) {
      const albums = this.albums$;
      let albumTitle = title;
      let match = title.match(/new\-(.*)/);
      const photos2add = this.photos.slice(this.idxStart);
      if (match !== null) {
         albumTitle = match[1];
      } else if (title = '') { // unused
         albumTitle = `default-${this.nameIdx++}`;
      } else {
         for (let album of albums) {
            if ( album.title.match(albumTitle) ) {
               this.currentAlbum = album; //currently unused
               // update the album with the new photos
               // some work is done by previously subscribed observer
               album.photos.push(...photos2add);
               // retrieve the album's proxy and update it
               const proxy = this.proxies[album.id];
               // const proxyItems2add = this.createProxy(photos2add);
               // this.sortedInsert<PhotoProxyItem>(proxy, this.compareLatLng, ...proxyItems2add);
               // proxy.mean= this.meanLatLngAccu(proxy, proxyItems2add);
               // just in case... but shouldn't be required
               // this.proxies[album.id] = proxy;
               this.idxStart = this.photos.length;
               return album.title;
            }
         }
      }
      // const album = {
      //    photos: new MyPhotos$(),
      //    title: albumTitle,
      //    id: this.albums.length
      // };
      const album = new MyAlbum(albums.length, albumTitle, new MyPhotos$());
      const proxy = <PhotoProxy>[];
      const thisService = this;
      album.photos.subject.subscribe({
         next: val => {
            const { photo, idx } = val;
            photo.album = albumTitle;
            photo.albumId = album.id;
            // const proxyItem = thisService.createProxy([photo], idx);
            // thisService.sortedInsert(proxy, thisService.compareLatLng, ...proxyItem);
            // proxy.mean = thisService.meanLatLngAccu(proxy, proxyItem);
         }
      });
      proxy.mean = { value: this.meanLatLng(photos2add), weight: photos2add.length };
      album.photos.push(...photos2add);

      const proxyObserver = {
         next: proxyItem => {
            thisService.sortedInsert(proxy, thisService.compareLatLng, ...proxyItem);
            proxy.mean = thisService.meanLatLngAccu(proxy, proxyItem);
         }
      }
      album.photos.subject.map(val => {
         const {photo, idx} = val;
         return thisService.createProxy([photo], idx);
      }).subscribe(proxyObserver);

      this.proxies[album.id]=proxy;
      // proxy.mean = { value: this.meanLatLng(photos2add), weight: photos2add.length };
      this.currentAlbum = album;
      this.idxStart = this.photos.length;
      albums.push(album);
      return albumTitle;
   }

   convert2Photodetails() {
      this.photos = this.photos.map(photo => {
         const {filename, url, exifData } = photo;
         return (<any>photo).getId !== undefined ? photo : new PhotoDetail(filename, url, exifData);
      });
   }

   computeAbumTitle(title: string) {
      let albumTitle: string;
      let match = title.match(/new\-(.*)/);
      if (match !== null) {
         albumTitle = match[1];
      } else if (title == '') { // unused
         albumTitle = `default-${this.nameIdx++}`;
      } else {
         albumTitle = title;
      }
      return albumTitle;
   }

   /**
    * DUPLICATE in google-maps.component
    */
    private convertGPSTags(lat: Array<Number>, latRef: string, long: Array<Number>, longRef: string): google.maps.LatLng {
        // lat : extends Number {denominator : Number, numerator : Number}
        let latDeg:number;
        let longDeg: number;
        //debugger;

        if( lat == undefined || long == undefined ){
            MapPage.showToast("Erreur, cette photo ne contient pas de position GPS", "middle", 2500);
            return undefined;
        }

        latDeg = lat[0].valueOf() + lat[1].valueOf() / 60 + lat[2].valueOf() / 3600;
        latDeg = latRef.search('N') == -1 ? -latDeg : latDeg;
        longDeg = long[0].valueOf() + long[1].valueOf() / 60 + long[2].valueOf() / 3600;
        longDeg = longRef.search('W') == -1 ? longDeg : -longDeg;

        return new google.maps.LatLng(latDeg,longDeg);
    }


    digestPhoto(image: File | DataURL): Promise<ImageWithExif> {
        const filename = image['name'] ? image['name'] : '';
        const exifPromise = this.promiseExifDataFromImg(image);
        let dataURLPromise : Promise<DataURL>;
        if (image instanceof Blob)
        dataURLPromise = this.readFilePromiseADataURL(image);
        else if (image['src'])
        dataURLPromise = Promise.resolve(image);
        return Promise.all<EXIF.ExifData, DataURL>([exifPromise, dataURLPromise]).then(([exifData, url]) => {
            const detailedImg = {
                exifData: exifData,
                url: url,
                filename: filename
            };
            //const imgForMap: ImageForMap = this.getImageForMap(detailedImg);
            //this.photos.push(imgForMap); //detailedImg);
            const photoDetail = new PhotoDetail( filename, url, exifData );
            if( photoDetail.latLng != undefined ){
                this.photos.push( photoDetail );
            }else{
                MapPage.showToast("Erreur l'image " + filename + " n'a pas de position", "top", 2500);
            }
            return photoDetail // detailedImg;
        });
    }

    private promiseExifDataFromImg(image) {
        return new Promise<EXIF.ExifData>((resolve, reject) => EXIF.getData(image, function () { resolve(this.exifdata) }));
    }

    /**
    * Promisify fileReader.readAsDataURL
    */
    private readFilePromiseADataURL(file: File|Blob) : Promise<DataURL> {
        const reader = new FileReader();

        return new Promise<DataURL>((resolve, reject) => {
            reader.onload = event => {
                const res: DataURL = (<FileReader>event.target).result;
                resolve(res);
            };
            reader.onerror = event => reject(reader.error);
            reader.readAsDataURL(file);
        });
    }



    private getImageForMap(img: ImageWithExif): ImageForMap {
        let res = <ImageForMap> {};
        const exifData = img.exifData;
        const latLng: google.maps.LatLng = this.convertGPSTags(exifData.GPSLatitude, exifData.GPSLatitudeRef, exifData.GPSLongitude, exifData.GPSLongitudeRef);
        res.filename = img.filename;
        res.url = img.url;
        res.latLng = latLng;
        res.album = null;
        res.albumId = null;
        res.exifData = exifData;
        return res;
    }

    private createProxy(photos: ImageForMap[], index?: number) : PhotoProxyItem[] {
        return photos.map((photo, idx) => {
            index = index === undefined ? idx : index;
            return { albumId: photo.albumId, idx: index, latLng: photo.latLng } as PhotoProxyItem;
        })
    }

    /**
    * Calculate average coordinates and associated weight from scratch.
    */
    private meanLatLng(photos: ImageForMap[]) {
        // create proxy to lighten the processing (sorting without the actual image)
        let proxy = this.proxies[photos[0].albumId];
        proxy = proxy ? proxy : <PhotoProxy> this.createProxy(photos);

        // TODO sort each time an item is added to the corresponding album
        //proxy.sort(this.compareLatLng);

        let mean = proxy.reduce<google.maps.LatLngLiteral>(
            (acc, val, idx) => {
                const ll = this.latLngLitteral2LatLng(val.latLng);
                const lat = acc.lat + (ll.lat() as number);
                const lng = acc.lng + (ll.lng() as number);
                return { lat: lat, lng: lng };

            }, { lat: 0, lng: 0 } as google.maps.LatLngLiteral);
            mean = {
                lat: mean.lat / proxy.length,
                lng: mean.lng / proxy.length
            };
            return new google.maps.LatLng(mean.lat, mean.lng);
        }

        /**
    * Calculate the average coordinates and weight from a previous set and with new additions.
    * Doesn't modify proxy given in parameter (stateless).
    * @param {photos} PhotoProxyItem[] newly added
    * @param {proxy} PhotoProxy to extract previous average and weight from
    * @return {mean} newly calculated average coordinates and combined weight
    */
   private meanLatLngAccu(proxy: PhotoProxy, photos: PhotoProxyItem[]): { value: google.maps.LatLng, weight: number} {
      let mean = photos.reduce(
         (acc, curr, currentIndex) => {
            const value = { lat: 0, lng: 0 };
            const weightNew = acc.weight + 1;
            let currLat, currLng;
            if (curr.latLng instanceof google.maps.LatLng) {
               currLat = curr.latLng.lat();
               currLng = curr.latLng.lng();
            } else {
               currLat = curr.latLng.lat;
               currLng = curr.latLng.lng;
            }
            value.lat = acc.value.lat * acc.weight + currLat;
            value.lat /= weightNew;
            value.lng = acc.value.lng * acc.weight + currLng;
            value.lng /= weightNew;
            return { value: value, weight: weightNew };
         }
         , {
            value: { lat: proxy.mean.value.lat(), lng: proxy.mean.value.lng() },
            weight: proxy.mean.weight
         }
      );

      return {
         value: new google.maps.LatLng(mean.value.lat, mean.value.lng),
         weight: mean.weight
      }
   }


   private compareLatLng(a: PhotoProxyItem, b: PhotoProxyItem): number {
      let ALat, ALng, BLat, BLng;
      if (a.latLng instanceof google.maps.LatLng) {
         ALat = a.latLng.lat();
         ALng = a.latLng.lng();

      } else {
         ALat = a.latLng.lat;
         ALng = <number>a.latLng.lng;
      }
      if (b.latLng instanceof google.maps.LatLng) {
         BLat = b.latLng.lat();
         BLng = b.latLng.lng();
      } else {
         BLat = <number>b.latLng.lat;
         BLng = <number>b.latLng.lng;
      }
      if (ALat == BLat)
         return ALng - BLng;
      else
         return ALat - BLat;
   }

   private sortedInsert<T>(arr: Array<T>, compare: (a: T, b: T) => number, ...items: T[]);
   private sortedInsert<T>(arr: Array<T>, compare: (a: T, b: T) => number, itemCreation: () => T[]);
   /**
    * Insert the item while maintaining the order infered by the compare function.
    * Not stable.
    * @param arr: array into which item is inserted, in place
    * @param item: the item to insert
    * @param compare: the comparison function, returns <0 if a<b, >0 otherwise
    */
   private sortedInsert<T>(arr: Array<T>, compare: (a: T, b: T) => number, ...items: any[]): number {
      if (items && items.length === 1 && typeof items[0] === 'function') {
         const itemCreation: () => T[] = items[0];
         items = itemCreation();
      }
      for( const item of items ) {
         for (let i = 0; i < arr.length; i++) {
            const a = arr[i];
            if (compare(item, a) <= 0) { // equality => not stable
               arr.splice(i, 0, item);
               return i;
            }
         }
         // arr is empty or item is greater than any item in arr
         return arr.push(item);
      }
   }



   getMean(album: Album) {
      return this.proxies[album.id].mean;
   }

   private latLngLitteral2LatLng(latLng: google.maps.LatLng | google.maps.LatLngLiteral) {
      if (latLng instanceof google.maps.LatLng) {
         return latLng;
      } else {
         return new google.maps.LatLng(latLng.lat, latLng.lng);
      }
   }

   private latLng2Litteral(latLng: google.maps.LatLng | google.maps.LatLngLiteral) {
      if (latLng instanceof google.maps.LatLng) {
         return { lat: latLng.lat(), lng: latLng.lng() };
      } else {
         return latLng;
      }
   }

   formatForSave(album: Album) {
      const meta: any = {
         size: album.photos.length,
         title: album.title
      };
      const photos: ImageForMap4DB[] = album.photos.map(val => {
         const res = <ImageForMap4DB>Object.assign({} as ImageForMap4DB, val);
         res.latLng = this.latLng2Litteral(val.latLng); //{ lat: (<google.maps.LatLng>val.latLng).lat(), lng: (<google.maps.LatLng>val.latLng).lng() };
         return res;
      });
      const key = album.id;

      return { key: key, meta: meta, photos: photos };
   }



   public save() {
      const obs = Observable.from(this._albums$);
      const data$ = obs.map(this.formatForSave.bind(this));
      return data$.concatMap(({meta, key, photos}) => {
         this.fromDB = false;
         return this.locDBService.updateAlbum(key, meta, photos);

         // this.rDBservice.updateAlbum(key, meta, photos);
      });

      //data$.subscribe();
   }
}

export class MyAlbum implements Album {
   constructor(public id, public title, public photos: MyPhotos$ = new MyPhotos$()) {

   }
   static fromPhotos(photos: ImageForMap[]) {
      const img = photos[0];
      const myPhotos = photos instanceof MyPhotos$ ? photos : new MyPhotos$();
      return new MyAlbum(img.albumId, img.album, myPhotos);
   }
   addPhotos(items: ImageForMap[]) {
      this.photos.push(...items);
   }
}

/**
 * Kind of an Album[] which kind-of-decorates a Subject<Album>, or the other way around...
 */
export class MyAlbums$ extends Array<Album>{
   photos;
   multicasted: Observable<Album>;

   constructor(public subject: Subject<Album>) {
      super();
      this.multicasted = subject.multicast(() => new ReplaySubject<Album>()).refCount();
      subject.subscribe({
         complete: () => {
            // debugger;
            const subject = new Subject<Album>();
            this.subject = subject;
            const obs = Observable.from(this);
            this.multicasted = subject.multicast(() => new ReplaySubject<Album>());
            (<ConnectableObservable<Album>>this.multicasted).connect();
            obs.subscribe({
               next: val => {
                  // debugger;
                  this.subject.next(val);
               }
            });
         }
      });
   };

   get multicasted1() {
      if (this.subject.isStopped) {
         const obs = Observable.from(this);
         this.subject = new Subject<Album>();
         obs.subscribe(this.subject);
         return this.multicasted = this.subject.multicast(() => new ReplaySubject<Album>()).refCount();
      } else
         return this.multicasted;
   }

   push(...items: Album[]): number {
      for (const album of items) {
         this.subject.next(album);
         super.push(album);
      }
      return this.length;
   }
}

/**
 * Kind of an ImageForMap[] which kind-of-decorates a Subject<ImageForMap>, or the other way around...
 */
export class MyPhotos$ extends Array<ImageForMap>{
   subject: Subject<{ photo: ImageForMap, idx: number }>
   constructor() {
      super();
      this.subject = new Subject<{ photo: ImageForMap, idx: number }>();
   };

   push(...items: ImageForMap[]): number {
      for (const photo of items) {
         const idx = super.push(photo);
         this.subject.next({ photo: photo, idx: idx });
      }
      return this.length;
   }
}

