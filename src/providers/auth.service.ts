import { Injectable } from '@angular/core';
import { AuthMethods, AuthProviders, FirebaseAuth, FirebaseAuthState } from 'angularfire2';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class AuthService {
   private authState: FirebaseAuthState;
   providers = [];

  constructor(public auth$: FirebaseAuth) {
   //   this.authState = auth$.getAuth();

     auth$.subscribe((state: FirebaseAuthState) => {
       this.authState = state;
    });
    for (const prov in AuthProviders) {
       this.providers.push(prov);
    }
  }

  get authenticated(): boolean {
    return this.authState !== null;
  }
  get uid() {
     return this.authenticated?this.authState.uid: null;
  }

  signInWithFacebook(): firebase.Promise<FirebaseAuthState> {
    return this.auth$.login({
      provider: AuthProviders.Facebook,
      method: AuthMethods.Popup
    });
  }
  signInWithGoogle(): firebase.Promise<FirebaseAuthState> {
     return this.auth$.login({
        provider: AuthProviders.Google,
        method: AuthMethods.Popup
      });
  }

  signOut(): void {
    this.auth$.logout();
  }

  displayName(): string {
    if (this.authState != null) {
      return this.authState.facebook.displayName;
    } else {
      return '';
    }
  }
}
