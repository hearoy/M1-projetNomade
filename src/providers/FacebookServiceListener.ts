export interface FacebookServiceListener {
    albumListFetched( json: string ): void;
    photoListFetched( json: string ): void;
    photoFetched( json: string): void;
    loginDone( json: string): void;
}
