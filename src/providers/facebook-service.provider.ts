import { Injectable } from '@angular/core';
import { Facebook, NativeStorage } from 'ionic-native';
import { MapPage } from '../pages/map/map';
import { FacebookServiceListener } from './FacebookServiceListener';

declare const FB:any;

@Injectable()
export class FacebookService {
    FB_APP_ID: number = 210083849437213;
    private listeners: FacebookServiceListener[];
    private accessToken;
    private expiresIn;
    private timeTokenGenerated;
    private init: boolean;
    public albumListAdded;


    constructor() {
        //Facebook.browserInit(this.FB_APP_ID, "v2.8");
        this.init = false;
        FB.init({
            appId      : this.FB_APP_ID,
            cookie     : false,  // enable cookies to allow the server to access
            // the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.8' // use graph api version 2.5
        });
        this.init = true;
        this.listeners = [];
        this.expiresIn = 0;
        this.albumListAdded = [];
    }

    public listen( listener: FacebookServiceListener): void{
        this.listeners.push( listener );
    }

    public isLogged(): boolean{
        if( this.accessToken == undefined ){
            return false;
        }else if( this.currentTimeSec() < this.timeTokenGenerated + this.expiresIn ){
            return true;
        }
        console.log( this.currentTimeSec() );
        console.log( this.timeTokenGenerated );
        console.log( this.expiresIn );
        var self = this;
        FB.getLoginStatus(function(response) {
            self.statusChangeCallback(response);
        });
        return false;
    }

    private currentTimeSec(): number{
        return new Date().getTime() / 1000;
    }

    statusChangeCallback(response) {
        console.log("FB: statusChangeCallback:" + response );
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            MapPage.showToast("Connexion avec Facebook ok", "top", 2500);
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            MapPage.showToast("Non autorisé, merci de réessayer", "top", 2500);
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            MapPage.showToast("Erreur, merci de vous connecter à Facebook", "top", 2500);
        }
    }

    doFbLogin( callback? ){
        let permissions = new Array();
        //the permissions your facebook app needs from the user
        permissions = [
            "public_profile",
            "user_friends",
            "user_photos"
        ];
        var self = this;
        FB.login(function(response) {
            if( response.status === 'connected'){
                self.timeTokenGenerated = self.currentTimeSec();
                self.accessToken = response.authResponse['accessToken'];
                self.expiresIn = response.authResponse.expiresIn;
                if( callback != undefined ){
                    callback( response );
                }
            }
        }, {scope: permissions});


    }

    getAlbumList( callback?, context? ){
        let request = "me";
        var self = this;
        FB.api(request, {fields: 'albums{cover_photo{images},created_time,name,photo_count,place}', access_token : this.accessToken}, function(response) {
            if( callback != undefined ){
                if( context != undefined ){
                    callback( response, context );
                }else{
                    callback( response );
                }
            }
            for(var l = 0; l < self.listeners.length; l++){
                self.listeners[ l ].albumListFetched( response );
            }
        });
    }


    getPhotoFromAlbumId(id: Number, callback?){
        //request to get all photo from one album
        //with the detail
        let request = id + "/photos";
        var self = this;
        FB.api(request, {access_token : this.accessToken, fields: 'images,id,place,name,created_time'}, function( response ) {
            if( callback != undefined ){
                callback( response );
            }
            for(var l = 0; l < self.listeners.length; l++){
                self.listeners[ l ].photoListFetched( response );
            }
        });
    }

    getPhotoFromId(id: Number, callback?){
        //request return an array of the image in differents sizes
        var self = this;
        FB.api(id + "", {access_token : this.accessToken, fields: 'picture,images,created_time,place'}, function(response) {

            if( callback != undefined ){
                callback( response );
            }
            for(var l = 0; l < self.listeners.length; l++){
                self.listeners[ l ].photoFetched( response );
            }
        });
    }

    getCoverImageFromId( id: Number, callback?){
        var self = this;
        FB.api(id + "", {access_token : this.accessToken, fields: 'images'}, function(response) {

            if( callback != undefined ){
                callback( response );
            }
        });

    }
}
