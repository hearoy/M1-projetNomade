import { Album, ImageForMap, MyAlbum, MyAlbums$, MyPhotos$ } from './detailed-photos.provider';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable, Subject } from 'rxjs';

export declare interface ImageForMap4DB extends ImageForMap {
   latLng: { lat: number, lng: number }
}
declare interface AlbumMeta {
   size: number,
   title: string,
   $key: string,
}


@Injectable()
export class LocalDBService {
   private _metas: string;
   private _albums: string;
   private metaArr = [];


   constructor( public auth: AuthService, private localStorage: Storage ) {

   }
   get albums() {
      return `/${this.auth.uid ? this.auth.uid : 'anon'}.albums.photos`;
   }
   get metas() {
      return `/${this.auth.uid ? this.auth.uid : 'user'}.albums.meta`;
   }

   getAlbumPath(key: string) {
      return `${this.albums}.${key}`;
   }
   getMetaPath(key: string) {
      return `${this.metas}.${key}`;
   }

   updateAlbum(key: number, meta: any, photos: ImageForMap4DB[]) {

      const metaVal = Object.assign({}, meta, {id:key});
      const aKey = this.getAlbumPath(key.toString());
      const mKey = this.getMetaPath(key.toString());
      const photoPromise = this.localStorage.set(aKey, photos);
      const metaPromise = this.localStorage.set(mKey, metaVal);
      this.metaArr.push(metaVal);
      const photo$ = Observable.fromPromise(photoPromise);
      const meta$ = Observable.fromPromise(metaPromise).do(()=>this.localStorage.set(this.metas, this.metaArr));

      return photo$.merge(meta$).map(val => { console.dir(val);console.log(`offlined.`)});
   }

   formatForSave(album) {
      let meta: any = {
         size: album.photos.length,
         title: album.title
      };
      let photos: ImageForMap4DB[] = album.photos.map(val => {
         const res = <ImageForMap4DB>Object.assign({} as ImageForMap4DB, val);
         res.latLng = this.latLng2Litteral(val.latLng); //{ lat: (<google.maps.LatLng>val.latLng).lat(), lng: (<google.maps.LatLng>val.latLng).lng() };
         return res;
      });
      return { meta: meta, photos: photos };
   }

   saveAlbum(album: Album) {

      const {meta, photos} = this.formatForSave(album);
      this.updateAlbum(album.id, meta, photos);


      //   let dbStruct = {
      //      uid.albums.meta: [
      //            { // albumId
      //               size: 3, // album.photos.length
      //               title: 'aze'
      //            }
      //          ]
      //         ,
      //         uid.albums.photos.0: [
      //            { // albumId
      //               0: {'...' : '...'}, // album.photos[0]
      //               1: {'...' : '...'}
      //            }
      //         ]
      //      }
      //   };
   }

   download() {
      let myAlbums = new MyAlbums$(new Subject<Album>());
      const convert2ImgForMap = (val: ImageForMap4DB) => {
         const res = <ImageForMap>val;
         res.latLng = this.latLngLitteral2LatLng(val.latLng);
         return res;
      };
      // get the keys of the albums, from the metadata'
      const metas$ = Observable.fromPromise(<Promise<AlbumMeta[]>>this.localStorage.get(this.metas))
         .do(metas=>this.metaArr=metas?metas:[]);
      const keys$intermediary = metas$.concatMap<AlbumMeta>(metaArr => {
         if (metaArr)
            return Observable.from(metaArr);
         else {
            // return Observable.throw(new Error('Nothing locally!'));
            return Observable.empty();
         }
      });
      const keys$ = keys$intermediary.catch((err, caught) => { myAlbums.subject.error(err); return Observable.throw(err) })
         //.do(val => { debugger }, error => { debugger;})
         .pluck<string>('id');
      // get the albums, from each key ~ [albums[photos... ]... ]
      const albums$$ = keys$.map(key => {
         return Observable.fromPromise(<Promise<ImageForMap4DB[]>>this.localStorage.get(this.getAlbumPath(key)))
      });
      const myPhotos$$ = albums$$.map(album$ => {
         // transform the albums of Image4MapDb into an observable of MyPhotos$
         const photos$ = album$.map(album => album.map(convert2ImgForMap))
            .map(album => {
            const myPhotos = new MyPhotos$();
            myPhotos.push(...album);
            return myPhotos;
         });
         return photos$;
      }).share();
      myPhotos$$.subscribe(myPhotos$ => {
         myPhotos$.subscribe({
            next: myPhotos => {
               const id = myPhotos[0].albumId;
               const album = myAlbums[id];
               if (album === undefined)
                  myAlbums.push(MyAlbum.fromPhotos(myPhotos));
               else {
                  album.photos.push(...myPhotos);
               }
            },
            error: error => { return console.log(error);}
         });
      }, error => myAlbums.subject.error(error),
      // ()=> {myAlbums.subject.complete()} // is called too soon
      );
      myPhotos$$.concatAll().subscribe({
         complete: () => { /*debugger*/;myAlbums.subject.complete();}
      });
      return myAlbums;
   }

   private latLng2Litteral(latLng: google.maps.LatLng | google.maps.LatLngLiteral) {
      if (latLng instanceof google.maps.LatLng) {
         return { lat: latLng.lat(), lng: latLng.lng() };
      } else {
         return latLng;
      }
   }
   private latLngLitteral2LatLng(latLng: google.maps.LatLng | google.maps.LatLngLiteral) {
      if (latLng instanceof google.maps.LatLng) {
         return latLng;
      } else {
         return new google.maps.LatLng(latLng.lat, latLng.lng);
      }
   }

}
