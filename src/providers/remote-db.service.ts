import { Album, ImageForMap, MyAlbum, MyAlbums$, MyPhotos$ } from './detailed-photos.provider';
import { AuthService } from './auth.service';
import { Injectable, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { AngularFire, FirebaseApp, FirebaseListObservable } from 'angularfire2';
import * as firebase from 'firebase';

export declare interface ImageForMap4DB extends ImageForMap {
   latLng: { lat: number, lng: number }
}
declare interface AlbumMeta {
   size: number,
   title: string,
   $key: string,
}


@Injectable()
export class RemoteDBService {
   private _metas: FirebaseListObservable<AlbumMeta[]>;
   private _albums: FirebaseListObservable<ImageForMap4DB[][]>;

   private init: boolean = false;
   private myAlbums: MyAlbums$;

   constructor(public af: AngularFire, public auth: AuthService) {//, @Inject(FirebaseApp) private firebApp : firebase.app.App) {

   }
   get albums() {
      return this._albums;
   }

   // storage() {
   //    this.firebApp.storage();
   // }

   private _initialize() {
      if (!this.init) {
         this._metas = this.af.database.list(`/${this.auth.uid}/albums/meta`, {
            query: { orderByChild: 'title' }
         });
         this._albums = this.af.database.list(`/${this.auth.uid}/albums/photos`, {
            query: { orderByKey: true }
         });
         this.init = true;
         return false;
      } else
         return this.init;
   }

   private createAlbumPath(key: string): FirebaseListObservable<ImageForMap4DB[]> {
      return this.af.database.list(`/${this.auth.uid}/albums/photos/${key}`);
   }

   private updateAlbum(key: number, meta: any, photos: ImageForMap[]) {
      // debugger;
      const afSetPromise = function(ref : firebase.database.Reference, value) { return new Promise((resolve, reject) => ref.set(value, a=> resolve(a)) );};

      const val = {};
      val[key.toString()] = photos;
      afSetPromise(this._albums.$ref.ref, val).then(_ => console.log('photos updated')).catch(_ => console.log('photos error'));
      //this._albums.update(key.toString(), photos).then(_ => console.log('photos updated')).catch(_ => console.log('photos error'));
      this._metas.update(key.toString(), meta).then(_ => console.log('meta updated')).catch(_ => console.log('meta error'));
   }

   saveAlbum(album: Album, idx: number) {
      this._initialize();
      let meta: any = {
         size: album.photos.length,
         title: album.title
      };
      let photos: ImageForMap4DB[] = album.photos.map(val => {
         const res = <ImageForMap4DB>Object.assign({} as ImageForMap4DB, val);
         res.latLng = this.latLng2Litteral(val.latLng); //{ lat: (<google.maps.LatLng>val.latLng).lat(), lng: (<google.maps.LatLng>val.latLng).lng() };
         return res;
      });
      this.updateAlbum(album.id, meta, photos);


      //   let dbStruct = {
      //      albums: {
      //         meta: {
      //            0: { // albumId
      //               size: 3, // album.photos.length
      //               title: 'aze'
      //            }
      //         },
      //         photos: {
      //            0: { // albumId
      //               0: {'...' : '...'}, // album.photos[0]
      //               1: {'...' : '...'}
      //            }
      //         }
      //      }
      //   };
   }

   download() {
      if (this._initialize())
         return this.myAlbums;
      let myAlbums = new MyAlbums$(new Subject<Album>());
      this.myAlbums = myAlbums;
      // debugger;
      const convert2ImgForMap = (val: ImageForMap4DB) => {
         // debugger;
         const res = <ImageForMap>val;
         res.latLng = this.latLngLitteral2LatLng(val.latLng);
         return res;
      };
      // get the keys of the albums, from the metadata'
      const keys$ = this._metas.take(1).pluck<string>('0', '$key');
      // get the albums, from each key ~ [albums[photos... ]... ]
      const albums$$ = keys$.map(key => this.createAlbumPath(key));
      const myPhotos$$= albums$$.map(album$ => {
         // transform the albums of Image4MapDb into an observable of MyPhotos$
         const photos$ = album$.map(arr => arr.map(convert2ImgForMap)).map(album => {
            const myPhotos = new MyPhotos$();
            myPhotos.push(...album);
            return myPhotos;
         });
         return photos$;
      });
      myPhotos$$.subscribe(myPhotos$ => {
         myPhotos$.subscribe(myPhotos => {
            const id = myPhotos[0].albumId;
            const album = myAlbums[id];
            if (album === undefined)
               myAlbums.push(MyAlbum.fromPhotos(myPhotos));
            else {
               album.photos.push(...myPhotos);
            }
         });
      });
      // const photos = keys$.mergeMap<ImageForMap4DB[]>(key => { debugger;return this.createAlbumPath(key).take(1) }); // pb: when a new key arrives, albums get mixed up
      // photos.map(albumPhotos => { debugger;return albumPhotos.map(convert2ImgForMap)})
      //    .map((photos: ImageForMap[]) => {
      //       const myPhotos = new MyPhotos$();
      //       myPhotos.push(...photos);
      //       return myPhotos;
      //    })
      //    .subscribe(function next(myPhotos) {
      //       myAlbums.push(MyAlbum.fromPhotos(myPhotos));
      //    });

      // this._albums.map((photos: ImageForMap4DB[][]) => photos[0].map(convert2ImgForMap))
      //    .map((photos: ImageForMap[]) => {
      //       const myPhotos = new MyPhotos$();
      //       myPhotos.push(...photos);
      //       return myPhotos;
      //    })
      //    .subscribe(function next(myPhotos) {
      //       myAlbums.push(MyAlbum.fromPhotos(myPhotos));
      //    });
      return myAlbums;
   }

   private latLng2Litteral(latLng: google.maps.LatLng | google.maps.LatLngLiteral) {
      if (latLng instanceof google.maps.LatLng) {
         return { lat: latLng.lat(), lng: latLng.lng() };
      } else {
         return latLng;
      }
   }
   private latLngLitteral2LatLng(latLng: google.maps.LatLng | google.maps.LatLngLiteral) {
      if (latLng instanceof google.maps.LatLng) {
         return latLng;
      } else {
         return new google.maps.LatLng(latLng.lat, latLng.lng);
      }
   }

}
