import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MyApp } from './app1.component';
import { MapPage } from '../pages/map/map';
import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { PhotosDetailsPage } from '../pages/photos-details/photos-details';
import { ListPage } from '../pages/list/list';
import { DetailedPhotosService } from '../providers/detailed-photos.provider';
import { FacebookService } from '../providers/facebook-service.provider';
import { FileManager } from '../providers/file-manager.provider';
import { InputFilesCmp } from '../components/input-files/input-files.component';
import { GoogleMapsCpnt } from '../components/google-maps/google-maps.component';
import { AddToAlbumPage } from '../pages/add-to-album/add-to-album.page';
import { AuthService } from '../providers/auth.service';
import { RemoteDBService } from '../providers/remote-db.service';
import { LocalDBService } from '../providers/local-db.service';
import { PhotoOverlayViewPage } from '../pages/photo-overlay-view/photo-overlay-view';
import { WindowRef } from '../providers/WindowRef';
import { AddPhotoFacebookPage } from '../pages/add-photo-facebook/add-photo-facebook.page'

import { AngularFireModule } from 'angularfire2';

var firebaseConfig = {
    apiKey: "AIzaSyAiOLsLQzKU8uv9qXNibin646wvYYb1pIY",
    authDomain: "m1-projetweb-fieldtrip.firebaseapp.com",
    databaseURL: "https://m1-projetweb-fieldtrip.firebaseio.com",
    storageBucket: "m1-projetweb-fieldtrip.appspot.com",
    messagingSenderId: "909699836991"
  };

@NgModule({
  declarations: [
    MyApp,
    HelloIonicPage,
    PhotosDetailsPage,
    MapPage,
    ItemDetailsPage,
    ListPage,
    GoogleMapsCpnt,
    InputFilesCmp,
    AddToAlbumPage,
    PhotoOverlayViewPage,
    AddPhotoFacebookPage
  ],
  imports: [
     IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapPage,
    HelloIonicPage,
    PhotosDetailsPage,
    ItemDetailsPage,
    ListPage,
    AddToAlbumPage,
    PhotoOverlayViewPage,
    AddPhotoFacebookPage
  ],
  providers: [
     AuthService,
     DetailedPhotosService,
     FacebookService,
     LocalDBService,
     RemoteDBService,
     Storage,
     WindowRef],
})
export class AppModule {}
