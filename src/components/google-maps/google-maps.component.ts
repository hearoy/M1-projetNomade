import {Component, Input, ViewChild} from '@angular/core';
import {Album, ImageWithExif, ImageForMap} from '../../providers/detailed-photos.provider';


@Component({
    selector: 'google-maps',
    templateUrl: 'google-maps.component.html'
})
export class GoogleMapsCpnt {

    @Input() mapsAPIUrl: string;
    @Input() APIKey: string;

    @ViewChild('mapContainer') mapContainer: HTMLElement;
    public map: google.maps.Map;
    private _scriptId: string = 'mapsAPI';

    constructor() {
    }

    private appendScript(): HTMLScriptElement {
        console.log('in MapPage, appendScript.');
        const script = document.createElement('script');
        script.src = this.mapsAPIUrl;
        script.async = true;
        script.id = this._scriptId;
        script.onload = this.initMap.bind(this);
        document.head.appendChild(script);
        return script;
    }


    public static convertGPSTags(lat: Array<Number>, latRef: string, long: Array<Number>, longRef: string): google.maps.LatLng {
        // lat : extends Number {denominator : Number, numerator : Number}
        let latDeg:number;
        let longDeg: number;
        //debugger;


        latDeg = lat[0].valueOf() + lat[1].valueOf() / 60 + lat[2].valueOf() / 3600;
        latDeg = latRef.search('N') == -1 ? -latDeg : latDeg;
        longDeg = long[0].valueOf() + long[1].valueOf() / 60 + long[2].valueOf() / 3600;
        longDeg = longRef.search('W') == -1 ? longDeg : -longDeg;

        return new google.maps.LatLng(latDeg,longDeg);
    }

      createMarker(img: ImageWithExif | ImageForMap, pos?: google.maps.LatLng, map: google.maps.Map = this.map) {
         if (img === null) {
            return new google.maps.Marker({ map: map, position: pos, title: 'a marker', animation: google.maps.Animation.BOUNCE });
         }
         const dataURL = img.url;
         const exifData = img.exifData;
         let position: google.maps.LatLng | google.maps.LatLngLiteral;
         if (!img['latLng']) {
            const lat = exifData['GPSLatitude'];
            const lng = exifData['GPSLongitude'];
            const latRef = exifData.GPSLatitudeRef;
            const longRef = exifData.GPSLongitudeRef;
            position = GoogleMapsCpnt.convertGPSTags(lat, latRef, lng, longRef)
         } else {
            position = (img as ImageForMap).latLng;
         }

        const getHeightFromWidth : (w:number, ratio : number) => number = (w, ratio)=> w/ratio;


        // the actual size of the image even if it was resized (different from its size when it was digitized)
        const origSize = {
            w: exifData['PixelXDimension'] as number,
            h: exifData['PixelYDimension'] as number,
            ratio: exifData['PixelXDimension'] / exifData['PixelYDimension']
        };

        // the image is displayed at scaledSize, but the icon still takes the image's origSize (the difference replaced by transparent area)
        const scaledSize = new google.maps.Size(100, getHeightFromWidth(100, origSize.ratio));
        // adjust the anchor to the bottom, middle of the newly-sized image
        const anchor = new google.maps.Point(scaledSize.width / 2, scaledSize.height);
        const labelFontSize = 10;
        const label: google.maps.MarkerLabel = {
            color: 'blue',
            fontFamily: 'Noto Sans',
            fontSize: `${labelFontSize}px`,
            text: 'Photo'
        };

        const gIcon: google.maps.Icon = {
            anchor: anchor,
            labelOrigin: new google.maps.Point(0, -10),
            scaledSize: scaledSize,
            url: dataURL
        };
        return new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            icon: gIcon,//dataURL,
            label: label,
            map: map,
            position: position,
            title: 'exifData.title',
            zIndex: 1
        });
    }
    createInfoWindow(img: ImageWithExif, details?): google.maps.InfoWindow {
     const date: string = img.exifData['DateTimeOriginal'];
     let other = details == undefined ? "" : this.detailsInListItems(details);
     let htmlContent = `<div class="info-window">
            <img src=${img.url} class="info-window__img"/>
            <div class="info-window__image-details>
               <ul class="exif__list">
                  <li class="exif__item">
                     ${date}
                  </li>
                  ${other}
               </ul>
            </div>
         </div>`;
      return new google.maps.InfoWindow({
         content: htmlContent
      })
  }

  private detailsInListItems(details: Map<string, string>) {
     let res = "";
     for (let keys = details.keys(), key = keys.next(); !key.done; key=keys.next()) {
        res += `<li>
         ${key.value} : ${details.get(key.value)}
         </li>`;
     }
     return res;
  }

    getGeolocation() {
        let infoWindow = new google.maps.InfoWindow();/*new google.maps.InfoWindow({
            map: this.map
        } as google.maps.InfoWindowOptions);*/

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                const pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude );

                infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                infoWindow.open(this.map);
                this.map.setCenter(pos);
                setTimeout(() => infoWindow.close(), 3000);
            });
        }
    }

    /**
    * Initialize the google maps API
    */
    private initMap() {
        console.log('in google-maps.component, initMap.');
        if (! this.map ) {
            const container = document.getElementById('map-container'); // this.mapContainer is null outside (for googleapis)
            this.map = new google.maps.Map(container, {
                center: {
                    lat: 47.845116,
                    lng: 1.939712
                },
                zoom: 8
            });
            this.getGeolocation();
        } else {
            console.log('in google-maps.component, mapEmbedded :');
            console.dir(this.map);
        }
    }

    /**
    * Initialize the whole component : append script, init googleMaps API etc.
    */
    init() {
        let scriptElt = <HTMLScriptElement>document.getElementById(this._scriptId);
        console.log( scriptElt );
        console.log( this._scriptId );
        if ( ! scriptElt ) {
            scriptElt = this.appendScript();
        } else {
            this.initMap();
            this.getGeolocation();
        }
    }

    /**
    * ionViewDidEnter isn't triggered by ionic
    * -> a simple component isn't integrated into ionic's lifecycle??
    */
    //
    // ionViewDidEnter() {
    //    debugger;
    //    this.init();
    // }

    openInfoWindow(marker: google.maps.Marker, infoWindow : google.maps.InfoWindow) {
        infoWindow.open(this.map, marker);
        google.maps.event
    }


   drawPolyline( path: google.maps.MVCArray | google.maps.LatLng[] | google.maps.LatLngLiteral[] ) {
      let opts: google.maps.PolylineOptions;
      opts = {
         path: path,
         strokeColor: '#FF9900',
         strokeOpacity: 1.0,
         strokeWeight: 2,
         geodesic: false
      };
      let poly = new google.maps.Polyline(opts);
      poly.setMap(this.map);
   }

}
