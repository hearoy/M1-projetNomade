import { Component, ViewChild, ElementRef, Input } from '@angular/core';
import { DataURL, PromiseOfArrayBufferAndDataURL } from '../../pages/map/map';

/*
  Generated class for the InputFiles component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
   selector: 'input-files',
   templateUrl: 'input-files.component.html'
})
export class InputFilesCmp {
   @ViewChild('inputElt') inputElt: ElementRef;
   @Input() accept: string = "image/*";
   @Input() multiple: boolean = true;
   @Input() hidden: boolean = true;

   constructor() {
   }


   /**
    * Previous version
    */
   getFromLocal(): Promise<PromiseOfArrayBufferAndDataURL[]> {//PromiseOfArrayBufferAndDataURL[] {//Promise<DataURL>[] {
      // const promiseOfFileList = this.promiseAFileList; // /!\ 'this' is later undefined in promiseOfFileList()

      /**
       * when the filelist promise is fulfilled, then for each file return a promise of the DataURL, one of the ArrayBuffer
       * and the filename is the platform supports it [not on mobile](https://developer.mozilla.org/en-US/docs/Web/API/File/name#Browser_compatibility)
       */
      return this.promiseAFileList().then(files => {
         const arrayOfPromisesObjects: PromiseOfArrayBufferAndDataURL[] = Array.prototype.map.call(files, (file: File) => {
            const filename = file.name ? file.name : '';
            return { dataURL: this.readFilePromiseADataURL(file), buffer: this.readFilePromiseAnArrayBuffer(file), filename: filename };
         });
         return arrayOfPromisesObjects;
      });
   }

   /**
    * Get a promise of an array of files from local, via an input[type="file"]
    * @return a promise of File[]
    */
   getFilesFromLocal(): Promise<File[]> {
      return this.promiseAFileList().then(filelist => {
         return Array.prototype.map.call(filelist, file => file);
      })
   }

   /**
    * FOR POTENTIAL FUTURE USE
    * Get a promise of an array of files as DataURL(base64) from local, via an input[type="file"]
    * @return a promise of File[]
    */
   getImagesFromLocal(): Promise<DataURL[]> {
      return this.getFilesFromLocal().then(files => files.map(this.readFilePromiseADataURL));
   }

   /**
    * Get a promise of a fileList from this.inputElt
    */
   private promiseAFileList(): Promise<FileList> {
      return new Promise<FileList>((resolve, reject) => {
         this.inputElt.nativeElement.onchange = (event) => {
            const input = <HTMLInputElement>event.target;
            resolve(input.files);
         };
         this.inputElt.nativeElement.click();
      });
   };

   /**
    * Promisify fileReader.readAsDataURL
    */
   private readFilePromiseADataURL(file: File) : Promise<DataURL> {
      const reader = new FileReader();

      return new Promise<DataURL>((resolve, reject) => {
         reader.onload = event => {
            const res: DataURL = (<FileReader>event.target).result;
            resolve(res);
         };
         reader.onerror = event => reject(reader.error);
         reader.readAsDataURL(file);
      });
   };

   /**
    * Promisify fileReader.readAsArrayBuffer
    */
   private readFilePromiseAnArrayBuffer(file: File) : Promise<ArrayBuffer> {
      const reader = new FileReader();

      return new Promise<ArrayBuffer>((resolve, reject) => {
         reader.onload = event => resolve((<FileReader>event.target).result);
         reader.onerror = _ => reject(reader.error);

         reader.readAsArrayBuffer(file);
      });
   }

}
