import { OverlayView } from '../../components/overlay-view/overlay-view.component';
import { PhotoDetail } from '../../components/overlay-view/PhotoDetail';
import { NgModule } from '@angular/core';
import { WindowRef } from '../../providers/WindowRef';
import { MapPage } from '../../pages/map/map';

export class Tools{

    photoList = [];
    lastPhotoVisible = [];
    currentPhotoVisible = [];
    overlayList = [];
    private map;
    private needUpdate = false;
    private flightPath;
    private lastUpdateZoom;

    constructor(private winRef: WindowRef,
                private mapPage: MapPage) {

    }

    getOverlayById( id ){
        for(var p = 0; p < this.overlayList.length; p++){
            if( this.overlayList[ p ].getId() == id ){
                return this.overlayList[ p ];
            }
        }
    }

    /**
    *   close every overlayList opened
    **/
    public updateOverlayListOpen(){
        for(var o = 0; o < this.overlayList.length; o++){
            if( this.overlayList[ o ].opened ){
                this.overlayList[ o ].reDraw( );
            }
        }
    }

    setMap( map ){
        this.map = map;
    }

    setPhotoList( list ){
        this.photoList = list;
        this.update( );
    }

    getPhotoList( ){
        return this.photoList;
    }

    public update( ){
        this.currentPhotoVisible = [];
        for(var p = 0; p < this.photoList.length; p++){
            this.currentPhotoVisible.push( this.photoList[ p ].getId() );
        }
        if( ! this.sameArray( this.currentPhotoVisible, this.lastPhotoVisible ) ){
            this.updatePhoto( this );
        }
    }

    public updateZoom( ){
        if( this.lastUpdateZoom == null ){
            this.lastUpdateZoom = this.map.getZoom( );
        }
        if( Math.abs( this.lastUpdateZoom - this.map.getZoom() ) >= 1){
            //nettoyage des miniatures actuelles
            for(var m = 0; m < this.overlayList.length; m++){
                this.overlayList[ m ].remove();
            }
            this.overlayList = [];
            this.overlayList.length = 0;
            this.updatePhoto( this );
        }
    }

    removeLine() {
        if( this.flightPath != null ){
            this.flightPath.setMap( null );
        }
    }

    public static sortOverlayViewByDate( overlayList: OverlayView[] ): OverlayView[]{
        var overlayListByDate = [];
        overlayListByDate.push( overlayList[ 0 ] );
        for( var o = 1; o < overlayList.length; o++){

            var currentDistance = overlayList[ o ].averageDateTime();

            for( var d = 0; d < overlayListByDate.length; d++ ){
                if( currentDistance < overlayListByDate[ d ].averageDateTime ){
                    //échange
                    var overlayTmp = overlayListByDate[ d ];
                    overlayListByDate[ d ] = overlayList[ o ];
                    if( d + 1 == overlayListByDate.length ){
                        //on insère le nouvel élément
                        //puisque y'a rien après
                        overlayListByDate[ d + 1 ] = overlayTmp;
                    }else if( d + 1 < overlayListByDate.length ){
                        //on doit tout décaler
                        //pour positioner l'autre élément
                        var lastOverlay = overlayListByDate[ d + 1 ];
                        var otherOverlayTmp;
                        for(var i = d + 2; i < overlayListByDate.length + 1; i++){
                            otherOverlayTmp = overlayListByDate[ i ];
                            overlayListByDate[ i ] = lastOverlay;
                            lastOverlay = otherOverlayTmp;
                        }
                        overlayListByDate[ d + 1 ] = overlayTmp;
                    }
                }else if( d == overlayListByDate.length - 1 ){
                    overlayListByDate.push( overlayList[ o ] );
                    break;
                }
            }

        }
        return overlayListByDate;
    }

    updatePhoto( self ){
        if( /** self.needUpdate && **/ self.map != undefined){
            self.lastPhotoVisible = self.currentPhotoVisible;
            self.lastUpdateZoom = self.map.getZoom( );
            self.clusturing( self.currentPhotoVisible, 1, self.map );
            self.removeLine( );


            if( self.overlayList.length > 1 ){
                //si on a plus d'un overlayView, on créer des lignes entres
                //on crée une liste de latlng dans l'ordre chronologique des
                //overlayView
                let overlayListByDate = Tools.sortOverlayViewByDate( self.overlayList );
                //on crée ensuite les lignes entres les overlays dans l'ordre
                var flightPlanCoordinates = [];
                for(var o = 0; o < overlayListByDate.length; o++){
                    flightPlanCoordinates.push( overlayListByDate[ o ].latlng );
                }
                self.flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    geodesic: true,
                    strokeColor: '#232323',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });

                self.flightPath.setMap( self.map );
            }

            self.needUpdate = false;
        }
        //self.winRef.nativeWindow.setTimeout( self.updatePhoto.bind(null, self), 1000);
    }


    sameArray(first, second){
        for(var f = 0; f < first.length; f++){
            if( second.indexOf( first[ f ] ) == -1 ){
                return false;
            }
        }
        return (first.length == second.length);
    }

    average( list ){
        var sum = 0;
        for(var s = 0; s < list.length; s++){
            sum += list[ s ];
        }
        return sum / list.length;
    }

    /**
    *    Calcul le centroid de la liste des latlng de chaque photo
    *    Retourne un google.maps.LatLng
    **/
    public static centroidOfPhotoList( list ): google.maps.LatLng{
        //console.log("centroidOfPhotoList");
        var lat = 0.0;
        var lng = 0.0;
        for(var p = 0; p < list.length; p++){
            lat += list[ p ].getLatLng().lat();
            lng += list[ p ].getLatLng().lng();
        }
        return new google.maps.LatLng( lat / list.length,
            lng / list.length
        );
    }

    /**
    *   this.photoList: PhotoDetail[], Liste de toutes les photos
    *    photoTmp: Number[], Id des photos à afficher
    *    orientation: Number(0, 1):
    *                    0: horizontal
    *                    1: vertical
    *    map: map google maps
    *    @return: Retourne une liste d'OverlayView
    **/
    clusturing( photoTmp, orientation, map){
        var width = 3.0;
        var height = 7.0;
        var nbreCentroids = width * height;
        var centroids = [];
        /**
        * lat range [-90, 90]
        * lng range [-180, 180]
        **/
        //on créer une grille virtuelle en fonction de du zoom
        //et de l'orientation du téléphone.
        if( orientation ){
            //vertical

        }else{
            //horizontal
            //reversing
            width = width + height;
            height = width - height;
            width = width - height;
        }

        //pour chaque case de la grille on trouve le centroids
        //en fonction de la position de chaque photo
        var nortWest = new google.maps.LatLng(
            map.getBounds().getNorthEast().lat(),
            map.getBounds().getSouthWest().lng()
        ); //OK
        var heightMark = map.getBounds().getNorthEast().lat() -
        map.getBounds().getSouthWest().lat();
        var widthMark = map.getBounds().getNorthEast().lng() -
        map.getBounds().getSouthWest().lng();
        for(var caseX = 0; caseX < width; caseX++){
            for(var caseY = 0; caseY < height; caseY++){
                //on séléctionne d'abord les photos présentes dans la case actuelle
                var northEast = new google.maps.LatLng(
                    nortWest.lat() - (caseY * (heightMark / height)),
                    nortWest.lng() + ((caseX+1) * (widthMark / width))
                );
                var southWest = new google.maps.LatLng(
                    nortWest.lat() - ((caseY+1) * (heightMark / height)),
                    nortWest.lng() + ((caseX) * (widthMark / width))
                );

                //création du LatLngBounds représentant le rectangle de la case
                let bounds = new google.maps.LatLngBounds(southWest, northEast);
                let listPhotoInCase = [];
                //on set si il y a un overlay en cours
                for(var p = 0; p < photoTmp.length; p++){
                    let currentOverlayView: OverlayView = this.getOverlayViewInBounds( bounds );
                    //console.log(listPhoto[ p ] );
                    if( bounds.contains( this.photoList[ photoTmp[ p ] ].getLatLng() ) ){
                        //Si une overlayView existe déjà dans cette case on ajoute
                        //simplement la photo à l'overlay, pas besoin d'ajouter dans
                        //la listPhotoInCase
                        if( currentOverlayView != undefined ){
                            if( ! currentOverlayView.containsPhoto( this.photoList[ photoTmp[ p ] ] ) ){
                                currentOverlayView.addPhoto( this.photoList[ photoTmp[ p ] ] );
                            }
                        }else{
                            //séléction des photos dans la case courante
                            listPhotoInCase.push( this.photoList[ photoTmp[ p ] ] );
                        }

                    }
                }
                if( listPhotoInCase.length > 0 ){
                    //une fois les photos de la case séléctionnées
                    //on calcul le centroid de toutes les photos dans listPhotoInCase
                    var currentCentroid = Tools.centroidOfPhotoList( listPhotoInCase );
                    new OverlayView(
                        currentCentroid,
                        map,
                        listPhotoInCase,
                        this
                    )
                }
            }
        }
    }

    /**
    *   Retourne l'overlayView dans la limite passée,
    *   Si aucun trouvé, retourne undefined
    **/
    getOverlayViewInBounds( bounds: google.maps.LatLngBounds ): OverlayView{
        for(var o = 0; o < this.overlayList.length; o++){
            if( bounds.contains( this.overlayList[ o ].latlng ) ){
                return this.overlayList[ o ];
            }
        }
        return undefined;
    }


}
