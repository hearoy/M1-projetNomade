var idPhoto = 0;
import EXIF from 'exif-js';
import { MapPage } from '../../pages/map/map';
import {ImageForMap} from '../../providers/detailed-photos.provider';
import { GoogleMapsCpnt } from '../google-maps/google-maps.component';

export class PhotoDetail implements ImageForMap{

    private id: Number;
    public latLng: google.maps.LatLng;
    public date: Date;
    public album: string;
    public albumId: number;
    public exifData: EXIF.ExifData;



    constructor( public filename:string,
        public url: string,
        exifDataOrLatLng: EXIF.ExifData | google.maps.LatLng,
        date?: Date) {

            if( exifDataOrLatLng instanceof google.maps.LatLng ){
                if( date != undefined ){
                    this.date = date;
                }else{
                    MapPage.showToast("Erreur, la date pour cette image n'est pas disponible.", "middle", 3000);
                    return;
                }
                this.latLng = exifDataOrLatLng;

            }else{
                this.exifData = exifDataOrLatLng;
                const lat = exifDataOrLatLng['GPSLatitude'];
                const lng = exifDataOrLatLng['GPSLongitude'];
                const latRef = exifDataOrLatLng.GPSLatitudeRef;
                const longRef = exifDataOrLatLng.GPSLongitudeRef;

                var dateStr =  exifDataOrLatLng.DateTime;
                if( lat != undefined && lng != undefined ){

                    this.latLng = GoogleMapsCpnt.convertGPSTags( lat, latRef, lng, longRef );


                }
                if( dateStr != undefined || dateStr != null){

                    var dateStrSplitted = dateStr.split( ":" );
                    var year = parseInt( dateStrSplitted[ 0 ] );
                    var month = parseInt( dateStrSplitted[ 1 ] ) - 1;
                    var day = parseInt( dateStrSplitted[ 2 ].split(" ")[ 0 ] );
                    var hour = parseInt( dateStrSplitted[ 2 ].split(" ")[ 1 ] );
                    var min = parseInt( dateStrSplitted[ 3 ] );
                    var sec = parseInt( dateStrSplitted[ 4 ] );
                    this.date = new Date(year, month, day, hour, min, sec);
                }else if( date != undefined ){
                    this.date = new Date();
                }
            }

            this.id = idPhoto++;
        }



        getUrl(){
            return this.url;
        }
        getId(){
            return this.id;
        }
        getLatLng(){
            return this.latLng;
        }
    }
