import { PhotoDetail } from './PhotoDetail';
import { Tools } from './Tools';

var overlayId = 0;

export class OverlayView extends google.maps.OverlayView{

    id;
    latlng;
    public photoList: PhotoDetail[];
    div;
    width;
    height;
    private tools;
    /**
    *   Set true if user has clicked on
    **/
    public opened = false;

    constructor(latlng, map, photoList, tools) {
        super();
        this.latlng = latlng;
        this.photoList = photoList;
        this.width = 34;
        this.height = 34;
        this.id = overlayId++;
        this.setMap( map );
        this.tools = tools;

        tools.overlayList.push( this );

        // Define a property to hold the image's div. We'll
        // actually create this div upon receipt of the onAdd()
        // method so we'll leave it null for now.
        this.div = null;
    }

    /**
    * on prend la date la plus récente
    * on fait la distance avec toutes les autres
    * @return la distance moyenne entre les dates de chaque photo
    **/
    averageDateTime(){
        var recentDate = this.photoList[ 0 ].date;
        for(var p = 0; p < this.photoList.length; p++){
            if( this.photoList[ p ].date > recentDate ){
                recentDate = this.photoList[ p ].date;
            }
        }
        //calcul des distances
        var sum = 0;
        for(var d = 0; d < this.photoList.length; d++){
            if( recentDate != this.photoList[ d ].date ){
                var timeDiff = Math.abs( recentDate.getTime() - this.photoList[ d ].date.getTime() );
                //var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                sum += timeDiff;
            }
        }
        return sum / this.photoList.length;
    }

    public addPhoto( photo: PhotoDetail ): void{
        this.photoList.push( photo );
        if( this.photoList.length <= 4 ){
            //besoin de redessiner, et update position
            this.latlng = Tools.centroidOfPhotoList( this.photoList );
            this.reDraw( );
        }
    }

    public containsPhoto( photo: PhotoDetail ): boolean{
        for(var p = 0; p < this.photoList.length; p++){
            if( this.photoList[ p ].filename == photo.filename ){
                return true;
            }
        }
        return false;
    }

    getId(){
        return this.id;
    }

    reDraw(){
        if( this.div ){
            this.div.innerHTML = "";
            this.draw();
        }
    }

    onAdd(){
        var self = this;

        var div = this.div;
        if ( ! div ) {
            self.opened = false;
            var element = document.getElementById( 'marker-' + this.id );
            if( element == undefined ){
                div = this.div = document.createElement('div');
                div.id = "marker-" + this.id;
            }else{
                div = this.div = element;
            }
            div.className = 'marker';

            var panes = this.getPanes();
            panes.overlayLayer.appendChild( div );
            div.innerHTML = "";

            //add element to clickable layer
            this.getPanes().overlayMouseTarget.appendChild(div);

            // Add a listener - we'll accept clicks anywhere on this div, but you may want
            // to validate the click i.e. verify it occurred in some portion of your overlay.
            google.maps.event.addDomListener(div, 'click', function() {
                self.tools.mapPage.goToOverlayViewPage( self.sortPhotoByDate( ) );
                /**
                var overlay = self.tools.getOverlayById( this.id.split("-")[ 1 ] );
                if( overlay != undefined ){
                    if( overlay.photoList.length > 0 ){
                        var photosByDate = overlay.sortPhotoByDate( );
                        var currentHeight = Math.min(
                            (Math.ceil( photosByDate.length / 2 ) * (130) + overlay.photoList.length * 8 ),
                            120 * 3  + 18
                        );
                        if( Math.ceil( photosByDate.length / 2 ) % 2 ){
                            self.width = 120;
                        }else{
                            self.width = 240;
                        }
                        this.style.width = self.width + "px";
                        this.style.height = currentHeight + "px";
                        this.innerHTML = '';
                        this.className += " marker-opened";
                        var content = "";
                        for(var p = 0; p < photosByDate.length; p++){
                            content += '<div style="background-color:white;margin:0px;padding:0px;width:209px;height:'+
                            Math.ceil( (photosByDate[ p ].length ) / 2 ) * 120  +'px;"><p>';
                            content += photosByDate[ p ][ 0 ].date.getDate();
                            content += '/' + (photosByDate[ p ][ 0 ].date.getMonth( ) + 1 );
                            content += '/' + photosByDate[ p ][ 0 ].date.getFullYear( );
                            content += '</p>';
                            for(var q = 0; q < photosByDate[ p ].length; q++){
                                content += '<img style="border:1px solid #232323;margin-left:3px;float:left;" width="99" height="100" src="' + photosByDate[ p ][ q ].getUrl() + '" />';
                            }
                            content += "</div><hr>";
                        }
                        this.innerHTML = content;
                        var point = overlay.getProjection().fromLatLngToDivPixel( overlay.latlng );
                        if ( point ) {
                            this.style.left = (point.x - self.width / 2) + 'px';
                            this.style.top = (point.y - currentHeight / 2) + 'px';
                        }
                        self.opened = true;
                    }
                }
                **/

                google.maps.event.trigger(self, 'click');


            });
        }
    }

    translate(element, x: Number, y:Number){
        element.style["-webkit-transform"] = "translate(" + x + "px, " + y + "px)";
        element.style["-moz-transform"] = "translate(" + x + "px, " + y + "px)";
        element.style["-ms-transform"] = "translate(" + x + "px, " + y + "px)";
        element.style["-o-transform"] = "translate(" + x + "px, " + y + "px)";
        element.style["transform"] = "translate(" + x + "px, " + y + "px)";
    }

    draw(){

        var self = this;

        var div = this.div;
        if ( div && div.innerHTML == "" ) {
            self.opened = false;
            div.className = 'marker';

            if( this.photoList.length > 1 ){
                this.width = 66;
            }else if( this.photoList.length == 1 ){
                this.width = 34;
            }

            if( this.photoList.length > 2 ){
                this.height = 66;
            }

            div.style.position = 'absolute';
            div.style.cursor = 'pointer';
            div.style.width = this.width + "px";
            div.style.height = this.height + "px";
            div.style.background = 'white';

            var content = '';
            if( this.photoList[ 0 ] != undefined ){
                content += '<img src="' + this.photoList[ 0 ].getUrl() + '" class="picture"></img>';
                //content += '<div class="picture" style="background-image:url(\'' + this.photoList[ 0 ].getUrl() + '\');"></div>'
            }
            if( this.photoList[ 1]  != undefined ){
                content += '<img src="' + this.photoList[ 1 ].getUrl() + '" class="picture"></img>';
                //content += '<div class="picture" style="background-image:url(\'' + this.photoList[ 1 ].getUrl() + '\');"></div>'
            }
            if( this.photoList[ 2 ] != undefined ){
                content += '<img style="margin-top:-1px;float:left;" src="' + this.photoList[ 2 ].getUrl() + '" class="picture"></img>';
                //content += '<div class="picture" style="margin-top:-1px;float:left;background-image:url(\'' + this.photoList[ 2 ].getUrl() + '\');"></div>'
            }
            if( this.photoList[ 3 ] != undefined ){
                content += '<img src="' + this.photoList[ 3 ].getUrl() + '" style="margin-top:-1px;"class="picture"></img>';
                //content += '<div class="picture" style="margin-top:-1px;background-image:url(\'' + this.photoList[ 3 ].getUrl() + '\');"></div>'
            }
            div.innerHTML = content;
        }
        var point = this.getProjection().fromLatLngToDivPixel( this.latlng );

        if (point) {
            this.div.style.left = (point.x - this.width / 2) + 'px';
            this.div.style.top = (point.y - this.height / 2) + 'px';
        }
    }

    /**
    *   Tri les photos par jour
    *   @return Array[Number][Photo]
    **/
    sortPhotoByDate( ){
        var photosByDate = [];
        photosByDate[ 0 ] = [];
        photosByDate[ 0 ].push( this.photoList[ 0 ] );
        for(var p = 1; p < this.photoList.length; p++){
            var done = false;
            for(var d = 0; d < photosByDate.length; d++){
                if( photosByDate[ d ][ 0 ].date.getDay() == this.photoList[ p ].date.getDay() &&
                photosByDate[ d ][ 0 ].date.getMonth() == this.photoList[ p ].date.getMonth() &&
                photosByDate[ d ][ 0 ].date.getFullYear() == this.photoList[ p ].date.getFullYear() ){
                    photosByDate[ d ].push( this.photoList[ p ] );
                    done = true;
                    break;
                }
            }
            if( ! done ){
                var newArray = [];
                newArray.push( this.photoList[ p ] );
                photosByDate.push( newArray );
            }
        }
        return photosByDate;
    }

    remove(){
        if ( this.div ) {
            var element = document.getElementById( 'marker-' + this.id );
            element.parentNode.removeChild( element );
        }
    };

    getPosition(){
        return this.latlng;
    };
};
