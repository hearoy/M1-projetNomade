declare namespace EXIF {
   export interface ImageWithData extends HTMLImageElement {
      exifdata: ExifData;
      iptcdata: any;
   }
   export interface ExifData {
      [tag: string]: any,
      GPSLatitude: Number[],
      GPSLongitude: Number[],
      GPSLatitudeRef: string,
      GPSLongitudeRef: string,
      PixelXDimension?: number,
      PixelYDimension?: number
      DateTime: string
   }
   //private function readExifData(data : DataView, start : number);
   export function getAllTags(img: HTMLImageElement): ExifData;
   export function getData(img: HTMLImageElement|File|Blob, callback: (img: ImageWithData) => any);
   export function getTag(img: HTMLImageElement, tag: string): string;
   export function readFromBinaryFile(file: ArrayBuffer): ExifData;
   var GPSTags : {[octal:string]:string};

}

declare module "exif-js" {
   export = EXIF;
}
